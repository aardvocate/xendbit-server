#!/bin/bash

rsync --exclude 'target' -avzh /home/xend/src/xendcoin root@192.243.108.129:/home/xend/src/
rsync --exclude 'target' -avzh /home/xend/src/xendcoin root@192.250.236.149:/home/xend/src/
rsync --exclude 'target' -avzh /home/xend/src/xendcoin root@192.243.108.175:/home/xend/src/
rsync --exclude 'target' -avzh /home/xend/src/xendcoin root@192.250.236.180:/home/xend/src/
rsync --exclude 'target' -avzh /home/xend/src/xendcoin root@192.250.236.103:/home/xend/src/

rsync -avzh /home/xend/src/xendcoin/target/Xendcoin-1.0.0.one-jar.jar root@192.243.108.129:/home/xend/apps/xend.jar
rsync -avzh /home/xend/src/xendcoin/target/Xendcoin-1.0.0.one-jar.jar root@192.250.236.149:/home/xend/apps/xend.jar
rsync -avzh /home/xend/src/xendcoin/target/Xendcoin-1.0.0.one-jar.jar root@192.243.108.175:/home/xend/apps/xend.jar
rsync -avzh /home/xend/src/xendcoin/target/Xendcoin-1.0.0.one-jar.jar root@192.250.236.180:/home/xend/apps/xend.jar
rsync -avzh /home/xend/src/xendcoin/target/Xendcoin-1.0.0.one-jar.jar root@192.250.236.103:/home/xend/apps/xend.jar

rsync -avzh /home/xend/.xendcoin root@192.243.108.129:/home/xend/
rsync -avzh /home/xend/.xendcoin root@192.250.236.149:/home/xend/
rsync -avzh /home/xend/.xendcoin root@192.243.108.175:/home/xend/
rsync -avzh /home/xend/.xendcoin root@192.250.236.180:/home/xend/
rsync -avzh /home/xend/.xendcoin root@192.250.236.103:/home/xend/

