/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.api.utils;

import com.xendbit.xendserver.models.BuyRequest;
import com.xendbit.xendserver.models.SendObject;
import com.xendbit.xendserver.models.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.commons.mail.resolver.DataSourceUrlResolver;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;
import com.xendbit.xendserver.models.utils.DAOUtils;

/**
 * @author aardvocate
 */
@SuppressWarnings("unchecked")
public class Utils {

    public static Properties p = DAOUtils.p;

    protected static final Logger LOGGER = Logger.getLogger(Utils.class.getName());

    public static void sendAnyString(SendObject sendObject) throws Exception {
        String content = sendObject.getBody();
        LOGGER.info(content);

        String subjectLine = sendObject.getSubject();

        sendEmail(sendObject.getToAddress(), content, subjectLine, "RegInfo");
    }

    public static void sendConfirmationEmail(User dbUser) throws Exception {
        File emailFile = new File("/etc/xendbit/confirmation_email.html");

        String name = dbUser.getSurName() + " " + dbUser.getFirstName() + " " + dbUser.getMiddleName();
        String link = Utils.p.getProperty("email.confirmation.url") + "?c=" + DigestUtils.md5Hex(dbUser.getEmailAddress());
        String content = new String(Files.readAllBytes(Paths.get(emailFile.getAbsolutePath())));
        content = content.replaceAll("#link", link).replaceAll("#name", name);
        LOGGER.info(content);

        String subjectLine = "Congratulations: Welcome to XendBit";

        sendEmail(dbUser.getEmailAddress(), content, subjectLine, name);

    }

    public static void sendBountyEmail(User reqUser) throws Exception {
        File emailFile = new File("/etc/xendbit/bounty_email.html");

        String content = new String(Files.readAllBytes(Paths.get(emailFile.getAbsolutePath())));
        content = content.replaceAll("#referalCode", reqUser.getRefCode());
        LOGGER.info(content);
        String subjectLine = "Congratulations: Welcome to Xendbit Bounty Program";
        sendEmail(reqUser.getEmailAddress(), content, subjectLine, "");
    }

    private static void sendEmail(String emailAddress, String content, String subjectLine, String name, String... ccEmails) throws Exception {
        ImageHtmlEmail email = new ImageHtmlEmail();

        URL url = new URL(p.getProperty("mail.url"));
        email.setDataSourceResolver(new DataSourceUrlResolver(url));

        email.setHostName(p.getProperty("mail.hostname"));
        email.setSmtpPort(Integer.parseInt(p.getProperty("mail.smtp.port")));
        email.setAuthenticator(new DefaultAuthenticator(p.getProperty("mail.auth.username"), p.getProperty("mail.auth.password")));
        email.setSSLOnConnect(Boolean.parseBoolean(p.getProperty("mail.ssl")));
        email.setFrom(p.getProperty("mail.from"));
        email.setSubject(subjectLine);
        email.addTo(emailAddress, name);

        if (ccEmails != null && Arrays.asList(ccEmails).size() > 0) {
            email.addCc(ccEmails);
        }
        email.setHtmlMsg(content);
        email.setDebug(true);

        email.send();
    }

    public static void sendNewBuyRequestEmail(BuyRequest buyRequest) throws IOException, Exception {
        File emailFile = new File("/etc/xendbit/new_buy_request.html");

        String content = new String(Files.readAllBytes(Paths.get(emailFile.getAbsolutePath())));
        content = content.replaceAll("#amount", buyRequest.getAmount() + "");
        content = content.replaceAll("#refCode", buyRequest.getReference());
        content = content.replaceAll("#btcValue", buyRequest.getBitcoinAmount() + "");
        content = content.replaceAll("#btcAddress", buyRequest.getBitcoinAddress());
        content = content.replaceAll("#bank", buyRequest.getBank());
        content = content.replaceAll("#acountNumber", buyRequest.getAccountNumber());

        LOGGER.info(content);

        String subjectLine = "New Buy Request";

        String[] emailAddresses = p.getProperty("new.buy.request.notification.emails").split(";");

        sendEmail(buyRequest.getUser().getEmailAddress(), content, subjectLine, buyRequest.getUser().getFullname(), emailAddresses);
    }

    public static void send2FACode(User requestUser) throws IOException, Exception {
        File emailFile = new File("/etc/xendbit/2facode.html");

        String content = new String(Files.readAllBytes(Paths.get(emailFile.getAbsolutePath())));
        content = content.replaceAll("#code", requestUser.getCode());
        LOGGER.info(content);

        String subjectLine = "2 Factor Authentication";

        sendEmail(requestUser.getEmailAddress(), content, subjectLine, "");
    }
}

//{"text": "Ethereum", "value": "ETH", "symbol": "ETH", "ticker_symbol":"ethereum", "xend.fees": 0.000625, "block.fees": 0.0008, "xend.address": "0x5b61c8d90ca057d707d3a615dcacb7f03d372bce", "multiplier": 1000000000000000000, "url": "https://rinkeby.infura.io/qxz5FjxeA5Mg2zt2ieOg", "contract": "0x2dd6856f2a90d05071a392acd160b98652d72bbb"},
