package com.xendbit.xendserver.api;

import com.xendbit.xendserver.api.utils.Utils;
import com.xendbit.xendserver.models.BuyRequest;
import com.xendbit.xendserver.models.SendObject;
import com.xendbit.xendserver.models.User;
import com.xendbit.xendserver.models.dao.BuyRequestDAO;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("api")
public class MailerAPI {

    protected static final Logger LOGGER = Logger.getLogger(MailerAPI.class.getName());

    @Path("send")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response send(SendObject sendObject) throws Exception {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        Utils.sendAnyString(sendObject);
        return Response.ok(res).build();
    }

    @Path("send-email")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendEmail(User requestUser) throws Exception {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        Utils.sendConfirmationEmail(requestUser);
        return Response.ok(res).build();
    }

    @Path("send-2fa")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response send2FACode(User requestUser) throws Exception {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        Utils.send2FACode(requestUser);
        return Response.ok(res).build();
    }

    @Path("send-new-buy-order/{refCode}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendNewBuyOrderEmail(@PathParam("refCode") String refCode) throws Exception {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        BuyRequest buyRequest = BuyRequestDAO.findByRefCode(refCode);
        if(buyRequest != null) {
            Utils.sendNewBuyRequestEmail(buyRequest);
        }
        
        return Response.ok(res).build();
    }

    @Path("send-bounty-email")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendBountyEmail(User reqUser) throws Exception {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        Utils.sendBountyEmail(reqUser);
        return Response.ok(res).build();
    }
}
