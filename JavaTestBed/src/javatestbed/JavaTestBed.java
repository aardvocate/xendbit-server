/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatestbed;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author aardvocate
 */
public class JavaTestBed {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TrialApplication();
    }

    public static void TrialApplication() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("loaded class");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:4417/web", "website", "@bsoluteXnd123!@#");
            System.out.println("created con");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
