/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.bitcoinapi.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;

import com.neemre.btcdcli4j.core.BitcoindException;
import com.neemre.btcdcli4j.core.CommunicationException;
import com.neemre.btcdcli4j.core.client.BtcdClient;
import com.neemre.btcdcli4j.core.client.BtcdClientImpl;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author aardvocate
 */
public class Utils {

    protected static final Logger LOGGER = Logger.getLogger(Utils.class.getName());
    public static Properties litecoinNodeConfig = null;
    public static Properties bitcoinNodeConfig = null;
    static ObjectMapper OM = new ObjectMapper();

    public static BtcdClient client;

    static {
        InputStream is = null;
        try {
            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
            CloseableHttpClient httpProvider = HttpClients.custom().setConnectionManager(cm).build();
            bitcoinNodeConfig = new Properties();
            is = new BufferedInputStream(new FileInputStream("/etc/xendbit/bitcoin_rpc_config.properties"));
            bitcoinNodeConfig.load(is);
            is.close();

            litecoinNodeConfig = new Properties();
            is = new BufferedInputStream(new FileInputStream("/etc/xendbit/litecoin_rpc_config.properties"));
            litecoinNodeConfig.load(is);
            is.close();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
    }

    public static CloseableHttpClient registerHttps() {
        int timeout = 60;
        RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();
        CloseableHttpClient client = null;
        try {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true)
                    .build();

            client = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setDefaultRequestConfig(config).build();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    private static String getURL(Properties config) {
        String url = "http://" + config.getProperty("node.bitcoind.rpc.user") + ":"
                + config.getProperty("node.bitcoind.rpc.password") + "@" + config.getProperty("node.bitcoind.rpc.host")
                + ":" + config.getProperty("node.bitcoind.rpc.port") + "/";

        LOGGER.log(Level.INFO, "Posting to URL: {0}", url);

        return url;
    }

    public static void importPrivateKey(Properties config, String privateKey, String address) {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        CloseableHttpClient httpProvider = HttpClients.custom().setConnectionManager(cm).build();

        try {
            client = new BtcdClientImpl(httpProvider, config);
        } catch (Exception e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
            e.printStackTrace();
        }

        try {
            LOGGER.info(String.format("PrivKey: [%s], Address: [%s]", privateKey, address));
            client.importPrivKey(privateKey, address, false);
        } catch (BitcoindException | CommunicationException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
            e.printStackTrace();
        }
    }

    public static int __importPrivKey(Properties config, Object... params) {
        LOGGER.info("Importing Private Keys....");
        CloseableHttpClient client = registerHttps();

        HttpPost postRequest = new HttpPost(getURL(config));
        postRequest.setHeader("content-type", "text/plain");

        HashMap<String, Object> fields = new HashMap<>();

        fields.put("method", "importprivkey");
        fields.put("params", params);

        try {
            String jsonString = OM.writeValueAsString(fields);
            LOGGER.log(Level.INFO, "Uploading params: {0}", jsonString);
            StringEntity entity = new StringEntity(jsonString);

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Got Response: {0}", s);
            return httpResponse.getStatusLine().getStatusCode();
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, ioe.getMessage(), ioe);
        }

        return -1;
    }

    public static String pushTx(Properties config, Object... params) {
        CloseableHttpClient client = registerHttps();

        HttpPost postRequest = new HttpPost(getURL(config));
        postRequest.setHeader("content-type", "text/plain");

        HashMap<String, Object> fields = new HashMap<>();

        fields.put("method", "sendrawtransaction");
        fields.put("params", params);

        try {
            String jsonString = OM.writeValueAsString(fields);
            StringEntity entity = new StringEntity(jsonString);

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(s);
            HashMap<String, Object> responseData = OM.readValue(s, HashMap.class);

            if (responseData.containsKey("result")) {
                return responseData.get("result").toString();
            } else if (responseData.containsKey("error")) {
                throw new IOException(responseData.get("error").toString());
            }
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, ioe.getMessage(), ioe);
        }

        return null;
    }

    public static List<Object> listUnspent(Properties config, Object... params) {
        CloseableHttpClient client = registerHttps();

        HttpPost postRequest = new HttpPost(getURL(config));
        postRequest.setHeader("content-type", "text/plain");

        HashMap<String, Object> fields = new HashMap<>();

        fields.put("method", "listunspent");
        fields.put("params", params);
        try {
            String jsonString = OM.writeValueAsString(fields);
            StringEntity entity = new StringEntity(jsonString);

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            System.out.println(s);
            HashMap<String, Object> responseData = OM.readValue(s, HashMap.class);

            if (responseData.containsKey("result")) {
                return (ArrayList<Object>) responseData.get("result");
            } else if (responseData.containsKey("error")) {
                throw new IOException(responseData.get("error").toString());
            }
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, ioe.getMessage(), ioe);
        }

        return null;
    }

    public static List<Object> getBlockchainInfo(Properties config) {
        CloseableHttpClient client = registerHttps();

        HttpPost postRequest = new HttpPost(getURL(config));
        postRequest.setHeader("content-type", "application/json");
        postRequest.setHeader("Accept", "application/json");

        HashMap<String, Object> fields = new HashMap<>();

        fields.put("method", "getblockchaininfo");
        try {
            String jsonString = OM.writeValueAsString(fields);
            StringEntity entity = new StringEntity(jsonString);
            entity.setContentType("application/json");

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, ioe.getMessage(), ioe);
        }

        return null;
    }
}
