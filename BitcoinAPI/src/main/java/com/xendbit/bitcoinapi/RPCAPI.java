/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.bitcoinapi;

import com.xendbit.bitcoinapi.utils.Utils;
import com.xendbit.xendserver.models.SendObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.xendbit.xendserver.models.dao.APIUserDAO;

/**
 *
 * @author aardvocate
 */
@Path("rpc")
@SuppressWarnings("unchecked")
public class RPCAPI {

    protected static final Logger LOGGER = Logger.getLogger(RPCAPI.class.getName());

    @Path("importprivkey/{privKey}/{address}/{wallet}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response importPrivateKey(@HeaderParam("apiKey") String apiKey, @PathParam("wallet") String wallet, @PathParam("privKey") String privKey, @PathParam("address") String address) throws IOException {
        LOGGER.info(String.format("PrivKey: [%s], Address: [%s], Wallet: [%s]", privKey, address, wallet));
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }
        res.put("response_code", 0);
        res.put("response_text", "success");

        Properties configs[] = new Properties[]{};
        if (wallet.toUpperCase().contains("BTC")) {
            configs = new Properties[]{
                Utils.bitcoinNodeConfig
            };
        } else if (wallet.toUpperCase().contains("LTC")) {
            configs = new Properties[]{};
        } else if (wallet.toUpperCase().contains("ALL")) {
            configs = new Properties[]{
                Utils.bitcoinNodeConfig,
                //Utils.litecoinNodeConfig
            };
        }

        for (Properties config : configs) {
            new Thread() {
                @Override
                public void run() {
                    Utils.importPrivateKey(config, privKey, address);
                }
                
            }.start();
            res.put("result", 200);
        }

        return Response.ok(res).build();
    }

    @Path("pushtx/{wallet}")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response pushTx(@HeaderParam("apiKey") String apiKey, @PathParam("wallet") String wallet, SendObject sendObject) throws IOException {
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }
        res.put("response_code", 0);
        res.put("response_text", "success");

        Properties config = new Properties();
        Object params[] = new Object[] {
            sendObject.getTransactionHex()
        };
        
        LOGGER.info(sendObject.getTransactionHex());

        if (wallet.toUpperCase().contains("BTC")) {
            config = Utils.bitcoinNodeConfig;
        } else if (wallet.toUpperCase().contains("LTC")) {
            config = Utils.litecoinNodeConfig;
        }

        res.put("result", Utils.pushTx(config, params));
        
        return Response.ok(res).build();
    }

    @Path("utxos/{address}/{wallet}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response UTXOs(@HeaderParam("apiKey") String apiKey, @PathParam("address") String address, @PathParam("wallet") String wallet) throws IOException {
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }
        res.put("response_code", 0);
        res.put("response_text", "success");

        Properties config = new Properties();
        Object params[] = new Object[]{
            2, 99999, new String[]{address}
        };

        if (wallet.toUpperCase().contains("BTC")) {
            config = Utils.bitcoinNodeConfig;
        } else if (wallet.toUpperCase().contains("LTC")) {
            config = Utils.litecoinNodeConfig;
        }

        res.put("result", Utils.listUnspent(config, params));
        return Response.ok(res).build();
    }

}
