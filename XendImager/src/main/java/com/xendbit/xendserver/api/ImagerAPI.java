package com.xendbit.xendserver.api;

import com.xendbit.xendserver.api.utils.Utils;
import com.xendbit.xendserver.models.SendObject;
import com.xendbit.xendserver.models.User;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.sql.SQLException;
import java.util.Base64;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import com.xendbit.xendserver.models.dao.APIUserDAO;
import com.xendbit.xendserver.models.dao.UserDAO;
import com.xendbit.xendserver.models.utils.BCrypt;
import static com.xendbit.xendserver.models.utils.DAOUtils.p;

import org.apache.commons.codec.digest.DigestUtils;

@Path("api")
public class ImagerAPI {

    protected static final Logger LOGGER = Logger.getLogger(ImagerAPI.class.getName());

    @Path("save-image")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response saveImage(User requestUser) throws FileNotFoundException, IOException {
        LOGGER.info("Called saveImage");
        String filename = DigestUtils.md5Hex(requestUser.getEmailAddress().getBytes());
        File f = new File(p.getProperty("blockchain.pictures.location"), filename);
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(requestUser.getIdImage().getBytes());
        fos.flush();

      try {
            java.nio.file.Path p = f.toPath();
            FileSystem fileSystem = p.getFileSystem();

            UserPrincipalLookupService service = fileSystem.getUserPrincipalLookupService();
            UserPrincipal userPrincipal = service.lookupPrincipalByName("root");
            System.out.println("Found UserPrincipal: " + userPrincipal);

            //changing owner
            Files.setOwner(p, userPrincipal);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
      
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");
        res.put("response", f.getName());

        return Response.ok(res).build();
    }

    @Path("get-image")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getImage(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, User requestUser) throws IOException {
        LOGGER.info("Called getImage");

        requestUser.setWalletCode(wallet);
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        SendObject sendObject = new SendObject();

        sendObject.setPassword(requestUser.getPassword());

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            try {
                String picturesLocation = Utils.p.getProperty("blockchain.pictures.location");

                String content = new String(Files.readAllBytes(Paths.get(picturesLocation, requestUser.getIdImage())));

                String[] parts = content.split(",");
                String imageString = parts[1].replace("\n", "");
                byte[] imageByte = Base64.getDecoder().decode(imageString);
                ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                BufferedImage image = ImageIO.read(bis);
                bis.close();

                ImageIO.write(image, "png", baos);
                byte[] imageData = baos.toByteArray();

                String base64String = Base64.getEncoder().encodeToString(imageData);
                res.put("result", base64String);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                res.put("response_text", "error");
                res.put("result", "Blockchain Account can not be found");
            }
        }

        return Response.ok(res).build();
    }

}
