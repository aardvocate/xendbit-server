/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.api.utils;


import java.util.*;
import java.util.logging.Logger;
import com.xendbit.xendserver.models.utils.DAOUtils;

/**
 * @author aardvocate
 */
@SuppressWarnings("unchecked")
public class Utils {

    public static Properties p = DAOUtils.p;

    protected static final Logger LOGGER = Logger.getLogger(Utils.class.getName());    
    
}

//{"text": "Ethereum", "value": "ETH", "symbol": "ETH", "ticker_symbol":"ethereum", "xend.fees": 0.000625, "block.fees": 0.0008, "xend.address": "0x5b61c8d90ca057d707d3a615dcacb7f03d372bce", "multiplier": 1000000000000000000, "url": "https://rinkeby.infura.io/qxz5FjxeA5Mg2zt2ieOg", "contract": "0x2dd6856f2a90d05071a392acd160b98652d72bbb"},
