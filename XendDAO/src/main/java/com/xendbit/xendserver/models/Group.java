package com.xendbit.xendserver.models;

public class Group {
	
	private String name;
	private String owner;
	private String commission;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public String getCommission() {
		return commission;
	}
	
	public void setCommission(String commission) {
		this.commission = commission;
	}
}
