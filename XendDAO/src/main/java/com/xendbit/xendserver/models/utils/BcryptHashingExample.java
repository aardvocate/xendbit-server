package com.xendbit.xendserver.models.utils;

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with this
 * work for additional information regarding copyright ownership. The ASF
 * licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class BcryptHashingExample {

    public static void main(String[] args) throws Exception {

//        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("EC");
//        SecureRandom secureRandom = SecureRandom.getInstanceStrong();
//
//        keyGenerator.initialize(256, secureRandom);
//
//        Signature signature = Signature.getInstance("SHA256withECDSA");
//
//        KeyPair pair = keyGenerator.generateKeyPair();
//        PrivateKey priv = pair.getPrivate();
//        PublicKey pub = pair.getPublic();
//
//        System.err.println("Private Key: " + priv.toString());
//        System.err.println("Public Key: " + pub.toString());
//
//        signature.initSign(priv);
//
//        String str = "This is string to sign";
//
//        byte[] strByte = str.getBytes("UTF-8");
//        signature.update(strByte);
//
//        byte[] realSig = signature.sign();
//
//        System.out.println("Signature: " + new BigInteger(1, realSig).toString(16));
//
//        Cipher cipher = Cipher.getInstance("RSA");
//        PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(priv.getEncoded());
//        KeyFactory privKeyFactory = KeyFactory.getInstance("EC");
//        PrivateKey encodedPriv = privKeyFactory.generatePrivate(privSpec);
//
//        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(pub.getEncoded());
//        KeyFactory pubKeyFactory = KeyFactory.getInstance("EC");
//        PublicKey encodedPub = pubKeyFactory.generatePublic(pubSpec);
//        
//        cipher.init(Cipher.ENCRYPT_MODE, encodedPriv);
//        
//        String encodedData = Base64.getEncoder().encodeToString(cipher.doFinal(str.getBytes()));
//        System.err.println(encodedData);
//        
//        cipher.init(Cipher.DECRYPT_MODE, encodedPub);
//        System.err.println(new String(cipher.doFinal(Base64.getDecoder().decode(encodedData.getBytes()))));
//        
//        new UserAPI().calculateChecksum("flag that extended header data attached override any corresponding ones found okay");
        //String originalPassword = "password";
        //String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword, BCrypt.gensalt(12));
        //System.out.println(generatedSecuredPasswordHash);

        boolean matched = BCrypt.checkpw("password", "$2a$12$.x7Bp.HSOxoAkKVf9TAwyeqdyGh91X7Fob36sO006agl9v/o69VUK");
        System.out.println(matched);
    }
}
