/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.models.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.xendbit.xendserver.models.utils.DAOUtils;

/**
 *
 * @author aardvocate
 */
public class PropertiesDAO {

    private static final Logger LOGGER = Logger.getLogger(PropertiesDAO.class.getName());
    private static final ObjectMapper OM = new ObjectMapper();

    private static Connection getConnection() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        String driver = "com.mysql.jdbc.Driver";
        Class.forName(driver).newInstance();
        String conUrl = DAOUtils.p.getProperty("db.url");
        String username = DAOUtils.p.getProperty("db.username");
        String password = DAOUtils.p.getProperty("db.password");

        // LOGGER.info(conUrl);
        // LOGGER.info(username);
        // LOGGER.info(password);
        return DriverManager.getConnection(conUrl, username, password);
    }

    public static final ArrayList<HashMap<String, String>> getProperties(int glId) throws IOException {
        String query = "SELECT VALUE FROM XB_GL WHERE GL_ID = ?";
        ArrayList<HashMap<String, String>> result = new ArrayList<>();
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, glId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HashMap<String, String> res = OM.readValue(rs.getString("VALUE"), HashMap.class);
                result.add(res);
            }
            return result;
        } catch (SQLException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    public static final void getAppData(Properties p) {
        String query = "SELECT * FROM XB_APPDATA";
        System.out.println(query);
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String key = rs.getString("NAME");
                String value = rs.getString("VALUE");
                p.setProperty(key, value);
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) {
            Logger.getLogger(PropertiesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
