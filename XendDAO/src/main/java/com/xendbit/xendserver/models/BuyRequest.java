package com.xendbit.xendserver.models;

import java.util.Date;

public class BuyRequest {

    private long id;
    private User user;
    private double amount;
    private String reference;   
    private Date requestTime;
    private String status;
    private double bitcoinAmount;
    private String bitcoinAddress;
    private String accountNumber;
    private String bank;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }
    

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public double getBitcoinAmount() {
        return bitcoinAmount;
    }

    public void setBitcoinAmount(double bitcoinAmount) {
        this.bitcoinAmount = bitcoinAmount;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBitcoinAddress() {
        return bitcoinAddress;
    }

    public void setBitcoinAddress(String bitcoinAddress) {
        this.bitcoinAddress = bitcoinAddress;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
}
