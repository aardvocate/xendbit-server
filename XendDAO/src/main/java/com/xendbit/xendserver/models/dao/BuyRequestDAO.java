/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.models.dao;

import com.xendbit.xendserver.models.BuyRequest;
import com.xendbit.xendserver.models.User;
import com.xendbit.xendserver.models.utils.DAOUtils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aardvocate
 */
public class BuyRequestDAO {

    private static final Logger LOGGER = Logger.getLogger(BuyRequestDAO.class.getName());

    private static Connection getConnection() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        String driver = "com.mysql.jdbc.Driver";
        Class.forName(driver).newInstance();
        String conUrl = DAOUtils.p.getProperty("db.url");
        String username = DAOUtils.p.getProperty("db.username");
        String password = DAOUtils.p.getProperty("db.password");
        return DriverManager.getConnection(conUrl, username, password);
    }

    public static List<BuyRequest> getUserBuyOrders(User dbUser) {
        String query = "SELECT * FROM XB_BUY_REQUEST WHERE USER_ID = ? ORDER BY REQUEST_TIME DESC";
        LOGGER.info(query);
        List<BuyRequest> buyRequests = new ArrayList<>();
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            ps.setLong(1, dbUser.getId());
            ResultSet rst = ps.executeQuery();
            while (rst.next()) {
                BuyRequest buyRequest = getBuyRequestFromResultSet(rst);
                buyRequests.add(buyRequest);
            }
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }

        return buyRequests;        
    }
    
    public static boolean updateBuyRequestStringColumn(long id, String column, String value) {
        String query = "UPDATE XB_BUY_REQUEST SET " + column + " = ? WHERE ID = ?";
        LOGGER.info(query);
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            int i = 0;
            ps.setString(++i, value);
            ps.setLong(++i, id);
            ps.executeUpdate();
            return true;
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }

        return false;
    }
    
    public static boolean addBuyRequest(BuyRequest buyRequest) {
        String query = "INSERT INTO XB_BUY_REQUEST "
                + "(USER_ID, AMOUNT, REFERENCE, STATUS, BTC_VALUE, BTC_ADDRESS, BANK_NAME, BANK_ACCOUNT_NUMBER) "
                + "VALUES (?,?,?,?,?,?,?,?)";
        LOGGER.info(query);
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            int i = 0;
            ps.setLong(++i, buyRequest.getUser().getId());
            ps.setDouble(++i, buyRequest.getAmount());
            ps.setString(++i, buyRequest.getReference());
            ps.setString(++i, buyRequest.getStatus());
            ps.setDouble(++i, buyRequest.getBitcoinAmount());
            ps.setString(++i, buyRequest.getBitcoinAddress());
            ps.setString(++i, buyRequest.getBank());
            ps.setString(++i, buyRequest.getAccountNumber());

            return ps.execute();
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }

        return false;
    }
    
    public static List<BuyRequest> findBuyRequestsByStatus(String status) {
        String query = "SELECT * FROM XB_BUY_REQUEST WHERE STATUS = ?";
        LOGGER.info(query);
        List<BuyRequest> buyRequests = new ArrayList<>();
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, status);
            ResultSet rst = ps.executeQuery();
            while (rst.next()) {
                BuyRequest buyRequest = getBuyRequestFromResultSet(rst);
                buyRequests.add(buyRequest);
            }
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }

        return buyRequests;
    }
    
    public static BuyRequest findByRefCode(String refCode) {
        String query = "SELECT * FROM XB_BUY_REQUEST WHERE REFERENCE = ?";
        LOGGER.info(query);
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, refCode);
            ResultSet rst = ps.executeQuery();
            if (rst.next()) {
                return getBuyRequestFromResultSet(rst);
            }
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }

        return null;
    }

    private static BuyRequest getBuyRequestFromResultSet(ResultSet rst) throws SQLException {
        BuyRequest buyRequest = new BuyRequest();
        buyRequest.setUser(UserDAO.findById(rst.getLong("USER_ID")));
        buyRequest.setAmount(rst.getDouble("AMOUNT"));
        buyRequest.setBitcoinAddress(rst.getString("BTC_ADDRESS"));
        buyRequest.setBitcoinAmount(rst.getDouble("BTC_VALUE"));
        buyRequest.setReference(rst.getString("REFERENCE"));
        buyRequest.setRequestTime(rst.getTimestamp("REQUEST_TIME"));
        buyRequest.setStatus(rst.getString("STATUS"));
        buyRequest.setId(rst.getLong("ID"));
        buyRequest.setBank(rst.getString("BANK_NAME"));
        buyRequest.setAccountNumber(rst.getString("BANK_ACCOUNT_NUMBER"));
        return buyRequest;
    }
}
