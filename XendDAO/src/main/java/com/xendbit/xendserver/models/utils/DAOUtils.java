package com.xendbit.xendserver.models.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import com.xendbit.xendserver.models.dao.PropertiesDAO;
import java.io.FileInputStream;

/**
 * * * @author aardvocate
 */
public class DAOUtils {

    public static Properties p = null;

    static {
        if (p == null) {
            try {
                p = new Properties();
                p.setProperty("db.username", "xend");
                p.setProperty("db.password", "r3ALL45tr0ngp@55w0rd");
                p.setProperty("db.url", "jdbc:mysql://lb.xendbit.net:4417/xend?zeroDateTimeBehavior=convertToNull");    
                p.load(new  FileInputStream("/etc/xendbit/xendbit.properties"));
            } catch(Exception e) {
                e.printStackTrace();
            }

            PropertiesDAO.getAppData(p);
        }
    }

    public static void main(String args[]) {
        String encrypted = BCrypt.encrypt("Hello World. Hello Friends");
        System.err.println(encrypted);
        String decrypted = BCrypt.decrypt(encrypted);
        System.err.println(decrypted);
    }
}
