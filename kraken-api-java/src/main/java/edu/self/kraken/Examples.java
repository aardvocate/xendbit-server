package edu.self.kraken;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import edu.self.kraken.api.KrakenApi;
import edu.self.kraken.api.KrakenApi.Method;

public class Examples {

    public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchAlgorithmException {

        KrakenApi api = new KrakenApi();
        api.setKey("h2GB3Qc19BHx09/+656Vt+WeJzZzgKQmxCZZoktI7UKnTmzq2kE9afVP"); // FIXME
        api.setSecret("fbmDIoTXI2JFZtArcLNWpGXygy71vqX2/bDFQo7zL4GPTvPhx5kjoDiZLvQofugbRY4z2SEXI5QaX4aKh4s2Ig=="); // FIXME

        String response;
        Map<String, String> input = new HashMap<>();
//
//        input.put("pair", "LTCUSD");
//        response = api.queryPublic(Method.TICKER, input);
//        System.out.println(response);
//        System.out.println("----------------------------------");
//        
//        input.clear();
//        input.put("pair", "XBTUSD");
//        response = api.queryPublic(Method.ASSET_PAIRS, input);
//        System.out.println(response);
//        System.out.println("----------------------------------");
//        
        input.clear();
        input.put("asset", "ZUSD");
        response = api.queryPrivate(Method.BALANCE, input);
        System.out.println(response);
    }
}
