/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.socket.models;

import javax.websocket.Session;

/**
 *
 * @author aardvocate
 */
public class Message {

    private final Session session;
    private final String message;

    public Message(Session session, String message) {
        this.session = session;
        this.message = message;
    }

    public Session getSession() {
        return session;
    }

    public String getMessage() {
        return message;
    }
}
