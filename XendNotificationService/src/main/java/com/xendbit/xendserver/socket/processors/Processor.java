/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.socket.processors;


/**
 *
 * @author aardvocate
 */
public class Processor {
//
//    private Map<String, User> sessionMap;
//    private final ObjectMapper MAPPER = new ObjectMapper();
//    private static final Logger LOGGER = Logger.getLogger(Processor.class.getName());
//    public static boolean THREADS_STARTED = false;
//
//    public Processor() {
//        LOGGER.log(Level.INFO, "Starting Processors: {0}", !THREADS_STARTED);
//        if (!THREADS_STARTED) {
//            startThreads();
//            THREADS_STARTED = true;
//        }
//    }
//
//    private void startThreads() {
//        ExecutorService service = Executors.newCachedThreadPool();
//        for (int i = 0; i < 10; i++) {
//            LOGGER.info("Starting Thread: " + i);
//            service.execute(new ProcessorThread());
//        }
//    }
//
//    private class ProcessorThread implements Runnable {
//
//        @Override
//        public void run() {
//            try {
//                LOGGER.info("Waiting for message...");
//                Message m = NotificationEndpoint.QUEUE.take();
//                processMessage(m);
//            } catch (Exception ex) {
//                Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//
//    private void processMessage(Message m) {
//        String mesg = m.getMessage();
//        LOGGER.info("Got Message: " + mesg);
//        LOGGER.info(mesg);
//        Session session = m.getSession();
//        String coded = new String(Base64.getDecoder().decode(mesg));
//        String part1Key = coded.substring(0, 5);
//        String part2Key = coded.substring(coded.length() - 5);
//        coded = coded.replace(part1Key, "").replace(part2Key, "");
//
//        String msg = new String(Base64.getDecoder().decode(coded));                
//        HashMap<String, Object> messageMap = MAPPER.readValue(msg, HashMap.class);
//
//        String action = messageMap.get("action").toString();
//
//        if (sessionMap == null) {
//            sessionMap = new HashMap<>();
//        }
//
//        LOGGER.info("Message Action: " + action);
//        switch (action) {
//            case "setEmailAddress":
//                String emailAddress = messageMap.get("emailAddress").toString();
//                User user = new User();
//                user.setSession(session);
//                user.setSessionId(session.getId());
//                user.setEmailAddress(emailAddress);
//
//                com.xendbit.xendserver.models.User debtor = UserDAO.findByColumn("email", emailAddress);
//
//                HashMap<String, String> notificationRequest = new HashMap<>();
//
//                sessionMap.put(emailAddress, user);
//
//                if (debtor != null) {
//                    settleDebts(debtor, notificationRequest);
//                }
//
//                notificationRequest.put("setEmailAddressResponse", "User Session Started Successfully");
//                session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                break;
//            case "startTrade":
//                //buyer to seller
//                String buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                com.xendbit.xendserver.models.User buyer = UserDAO.findByColumn("email", buyerEmailAddress);
//
//                notificationRequest = new HashMap<>();
//                if (!buyer.isApproved()) {
//                    notificationRequest.put("action", "accountNotApproved");
//                    notificationRequest.put("message", "You account has not been approved by the Administrator. You can't buy coins on XendBit until your account has been approved");
//                    sessionMap.get(buyerEmailAddress).getSession().getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    break;
//                }
//
//                String trxId = messageMap.get("trxId").toString();
//
//                String buyerOtherAddress = messageMap.get("buyerOtherAddress").toString();
//
//                String buyerAddress = messageMap.get("buyerAddress").toString();
//                Exchange exchange = ExchangeDAO.findByTransactionId(trxId);
//
//                String sellerEmailAddress = exchange.getSeller().getEmailAddress();
//
//                User seller = sessionMap.get(sellerEmailAddress);
//
//                if (seller == null) {
//                    notificationRequest.put("action", "notOnline");
//                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    break;
//                }
//
//                Session sellerSession = seller.getSession();
//
//                if (!sellerSession.isOpen()) {
//                    notificationRequest.put("action", "notOnline");
//                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    break;
//                }
//
//                if (exchange.getStatus().equals(ExchangeDAO.STATUS.ORDER_PLACED.toString())) {
//                    notificationRequest.put("trxId", trxId);
//                    notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
//                    notificationRequest.put("buyerAddress", buyerAddress);
//                    notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
//                    notificationRequest.put("action", "startTrade");
//                    notificationRequest.put("amountToSell", "" + exchange.getAmountToSell());
//                    notificationRequest.put("fromCoin", "" + exchange.getFromCoin());
//                    notificationRequest.put("toCoin", "" + exchange.getToCoin());
//                    notificationRequest.put("message", "A buyer is interested in buying your " + exchange.getFromCoin() + " @");
//
//                    sellerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                } else {
//                    notificationRequest = new HashMap<>();
//                    notificationRequest.put("action", "cancelTrade");
//                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                }
//                break;
//            case "cancelTrade":
//                //seller to buyer
//                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                Session buyerSession = sessionMap.get(buyerEmailAddress).getSession();
//                notificationRequest = new HashMap<>();
//                notificationRequest.put("action", "cancelTrade");
//                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                break;
//            case "cancelBankPayment":
//                trxId = messageMap.get("trxId").toString();
//                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.ORDER_PLACED.toString());
//            case "askBuyerToPay":
//                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                buyer = UserDAO.findByColumn("email", buyerEmailAddress);
//                trxId = messageMap.get("trxId").toString();
//                buyerAddress = messageMap.get("buyerAddress").toString();
//                buyerOtherAddress = messageMap.get("buyerOtherAddress").toString();
//
//                exchange = ExchangeDAO.findByTransactionId(trxId);
//                String sellerOtherAddress = exchange.getSellerToAddress();
//
//                notificationRequest = new HashMap<>();
//                notificationRequest.put("trxId", trxId);
//                notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
//                notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
//                notificationRequest.put("sellerOtherAddress", sellerOtherAddress);
//                notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
//                notificationRequest.put("amountToSell", exchange.getAmountToSell() + "");
//                notificationRequest.put("toCoin", "" + exchange.getToCoin());
//                notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
//                notificationRequest.put("action", "paySeller");
//
//                exchange.setBuyerId(buyer.getId());
//                exchange.setBuyerToAddress(buyerOtherAddress);
//                exchange.setBuyerFromAddress(buyerAddress);
//                ExchangeDAO.updateBuyerInfo(exchange, ExchangeDAO.STATUS.SELLER_INTERESTED.toString());
//                if (sessionMap.containsKey(buyerEmailAddress)) {
//                    buyerSession = sessionMap.get(buyerEmailAddress).getSession();
//                    if (buyerSession.isOpen()) {
//                        buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    }
//                }
//                break;
//            case "sellerSentCoins":
//                //seller to buyer
//                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                buyer = UserDAO.findByColumn("email", buyerEmailAddress);
//                trxId = messageMap.get("trxId").toString();
//                buyerAddress = messageMap.get("buyerAddress").toString();
//                buyerOtherAddress = messageMap.get("buyerOtherAddress").toString();
//
//                exchange = ExchangeDAO.findByTransactionId(trxId);
//                sellerOtherAddress = exchange.getSellerToAddress();
//
//                notificationRequest = new HashMap<>();
//                notificationRequest.put("trxId", trxId);
//                notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
//                notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
//                notificationRequest.put("sellerOtherAddress", sellerOtherAddress);
//                notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
//                notificationRequest.put("toCoin", "" + exchange.getToCoin());
//                notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
//                notificationRequest.put("action", "sendCoinsToSeller");
//
//                exchange.setBuyerId(buyer.getId());
//                exchange.setBuyerToAddress(buyerOtherAddress);
//                exchange.setBuyerFromAddress(buyerAddress);
//                ExchangeDAO.updateBuyerInfo(exchange, ExchangeDAO.STATUS.SELLER_SENT_COINS.toString());
//                if (sessionMap.containsKey(buyerEmailAddress)) {
//                    buyerSession = sessionMap.get(buyerEmailAddress).getSession();
//                    if (buyerSession.isOpen()) {
//                        buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    }
//                }
//                break;
//            case "buyerSentCoins":
//                //buyer to seller
//                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                trxId = messageMap.get("trxId").toString();
//                exchange = ExchangeDAO.findByTransactionId(trxId);
//                sellerEmailAddress = exchange.getSeller().getEmailAddress();
//
//                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.SUCCESS.toString());
//
//                notificationRequest = new HashMap<>();
//                notificationRequest.put("action", "success");
//
//                buyerSession = sessionMap.get(buyerEmailAddress).getSession();
//                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//
//                if (sessionMap.containsKey(sellerEmailAddress)) {
//                    sellerSession = sessionMap.get(sellerEmailAddress).getSession();
//                    if (sellerSession.isOpen()) {
//                        sellerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    }
//                }
//                break;
//            case "buyerConfirmedBankPayment":
//                //buyer to seller
//                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                buyer = UserDAO.findByColumn("email", buyerEmailAddress);
//                HashMap<String, Object> kyc = getAccountKYC(buyer);
//                String buyerFullname = kyc.get("name") == null ? "Unknown User" : kyc.get("name").toString();
//                trxId = messageMap.get("trxId").toString();
//                exchange = ExchangeDAO.findByTransactionId(trxId);
//                sellerEmailAddress = exchange.getSeller().getEmailAddress();
//
//                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.SUCCESS.toString());
//
//                notificationRequest = new HashMap<>();
//                notificationRequest.put("action", "buyerConfirmedBankPayment");
//                notificationRequest.put("buyerFullname", buyerFullname);
//                notificationRequest.put("trxId", trxId);
//                notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
//                notificationRequest.put("fromCoin", exchange.getFromCoin());
//
//                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.BUYER_PAID.toString());
//
//                if (sessionMap.containsKey(sellerEmailAddress)) {
//                    sellerSession = sessionMap.get(sellerEmailAddress).getSession();
//                    if (sellerSession.isOpen()) {
//                        sellerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    }
//                }
//                break;
//            case "errorSendingToSeller":
//                //buyer to seller
//                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                trxId = messageMap.get("trxId").toString();
//                exchange = ExchangeDAO.findByTransactionId(trxId);
//
//                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.BUYER_SENDING_ERROR.toString());
//
//                notificationRequest = new HashMap<>();
//                notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
//                notificationRequest.put("coin", "" + exchange.getToCoin());
//                notificationRequest.put("amount", "" + exchange.getAmountToRecieve());
//                notificationRequest.put("action", "errorSendingToSeller");
//
//                buyerSession = sessionMap.get(buyerEmailAddress).getSession();
//                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                break;
//            case "errorSendingToBuyer":
//                //seller to buyer
//                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
//                trxId = messageMap.get("trxId").toString();
//
//                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.BUYER_SENDING_ERROR.toString());
//
//                notificationRequest = new HashMap<>();
//                notificationRequest.put("action", "cancelTrade");
//
//                buyerSession = sessionMap.get(buyerEmailAddress).getSession();
//                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                break;
//            case "askBeneficiaryForAddress":
//                notificationRequest = new HashMap<>();
//                String donorEmailAddress = messageMap.get("donorEmailAddress").toString();
//                String beneficiaryEmailAddress = messageMap.get("beneficiaryEmailAddress").toString();
//
//                User beneficiarySessionUser = sessionMap.get(beneficiaryEmailAddress);
//
//                if (beneficiarySessionUser == null) {
//                    notificationRequest.put("action", "notOnline");
//                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    break;
//                }
//
//                Session beneficiarySession = beneficiarySessionUser.getSession();
//                if (!beneficiarySession.isOpen()) {
//                    notificationRequest.put("action", "notOnline");
//                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    break;
//                }
//
//                com.xendbit.xendserver.models.User donor = UserDAO.findByColumn("email", donorEmailAddress);
//
//                kyc = getAccountKYC(donor);
//                String donorFullname = kyc.get("name").toString();
//
//                notificationRequest.put("action", "provideAddressToDonor");
//                notificationRequest.put("coin", messageMap.get("coin").toString());
//                notificationRequest.put("donor", donorFullname);
//                notificationRequest.put("donorEmailAddress", donorEmailAddress);
//                beneficiarySession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                break;
//            case "provideAddressToDonor":
//                notificationRequest = new HashMap<>();
//                donorEmailAddress = messageMap.get("donorEmailAddress").toString();
//                User donorSessionUser = sessionMap.get(donorEmailAddress);
//
//                if (donorSessionUser == null) {
//                    notificationRequest.put("action", "notOnline");
//                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    break;
//                }
//
//                Session donorSession = donorSessionUser.getSession();
//                if (!donorSession.isOpen()) {
//                    notificationRequest.put("action", "notOnline");
//                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//                    break;
//                }
//
//                notificationRequest.put("action", "addressProvidedToDonor");
//                notificationRequest.put("coin", messageMap.get("coin").toString());
//                notificationRequest.put("address", messageMap.get("address").toString());
//                donorSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//
//                break;
//            default:
//                LOGGER.info("Invalid Message Recieved");
//                break;
//        }
//    }
//
//    private void settleDebts(com.xendbit.xendserver.models.User user, final HashMap<String, String> notificationRequest) {
//        List<Exchange> sellOrders = ExchangeDAO.getDebts(user);
//
//        sellOrders.stream().forEach(x -> {
//            LOGGER.info(x.getStatus());
//            String buyerEmailAddress = user.getEmailAddress();
//            com.xendbit.xendserver.models.User buyer = null;
//            try {
//                buyer = UserDAO.findByColumn("email", buyerEmailAddress);
//            } catch (SQLException e) {
//                LOGGER.log(Level.SEVERE, e.getMessage(), e);
//            }
//            if(buyer == null) {
//                return;
//            }
//            String trxId = x.getTrxId();
//            String buyerAddress = x.getBuyerFromAddress();
//            String buyerOtherAddress = x.getBuyerToAddress();
//
//            Exchange exchange = ExchangeDAO.findByTransactionId(trxId);
//            String sellerOtherAddress = exchange.getSellerToAddress();
//
//            notificationRequest.put("trxId", trxId);
//            notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
//            notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
//            notificationRequest.put("sellerOtherAddress", sellerOtherAddress);
//            notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
//            notificationRequest.put("toCoin", "" + exchange.getToCoin());
//            notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
//            notificationRequest.put("action", "sendCoinsToSeller");
//
//            exchange.setBuyerId(buyer.getId());
//            exchange.setBuyerToAddress(buyerOtherAddress);
//            exchange.setBuyerFromAddress(buyerAddress);
//            ExchangeDAO.updateBuyerInfo(exchange, ExchangeDAO.STATUS.SELLER_SENT_COINS.toString());
//            Session buyerSession = sessionMap.get(buyerEmailAddress).getSession();
//            try {
//                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
//            } catch (IOException e) {
//                LOGGER.log(Level.SEVERE, e.getMessage(), e);
//            }
//        });
//    }
}
