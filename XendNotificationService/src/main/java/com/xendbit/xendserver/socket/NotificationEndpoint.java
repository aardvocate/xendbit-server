package com.xendbit.xendserver.socket;

import com.xendbit.xendserver.models.Exchange;
import com.xendbit.xendserver.models.dao.ExchangeDAO;
import com.xendbit.xendserver.models.dao.UserDAO;
import org.bitcoinj.wallet.UnreadableWalletException;
import org.codehaus.jackson.map.ObjectMapper;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

@SuppressWarnings("unchecked")
@ServerEndpoint("/websocket")
public class NotificationEndpoint {

    private static final Logger LOGGER = Logger.getLogger(NotificationEndpoint.class.getName());
    private static Map<String, User> sessionMap;
    private static final ObjectMapper MAPPER = new ObjectMapper();

    static {
        LOGGER.info("Starting Notification Websocket");
    }

    private static void settleDebts(com.xendbit.xendserver.models.User user, final HashMap<String, String> notificationRequest) {
        List<Exchange> sellOrders = ExchangeDAO.getDebts(user);

        sellOrders.stream().forEach(x -> {
            LOGGER.info(x.getStatus());
            String buyerEmailAddress = user.getEmailAddress();
            com.xendbit.xendserver.models.User buyer = null;
            try {
                buyer = UserDAO.findByColumn("email", buyerEmailAddress);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            String trxId = x.getTrxId();
            String buyerAddress = x.getBuyerFromAddress();
            String buyerOtherAddress = x.getBuyerToAddress();

            Exchange exchange = ExchangeDAO.findByTransactionId(trxId);
            String sellerOtherAddress = exchange.getSellerToAddress();

            notificationRequest.put("trxId", trxId);
            notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
            notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
            notificationRequest.put("sellerOtherAddress", sellerOtherAddress);
            notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
            notificationRequest.put("toCoin", "" + exchange.getToCoin());
            notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
            notificationRequest.put("action", "sendCoinsToSeller");

            exchange.setBuyerId(buyer.getId());
            exchange.setBuyerToAddress(buyerOtherAddress);
            exchange.setBuyerFromAddress(buyerAddress);
            ExchangeDAO.updateBuyerInfo(exchange, ExchangeDAO.STATUS.SELLER_SENT_COINS.toString());
            Session buyerSession = sessionMap.get(buyerEmailAddress).getSession();
            try {
                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @OnOpen
    public void onOpen(final Session session, EndpointConfig config) {
        LOGGER.info("Socket Opened");
    }

    @OnMessage
    public void onMessage(final Session session, String mesg) throws IOException, SQLException, BadPaddingException, InterruptedException, UnreadableWalletException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, ClassNotFoundException {
        String coded = new String(Base64.getDecoder().decode(mesg));
        String part1Key = coded.substring(0, 5);
        String part2Key = coded.substring(coded.length() - 5);
        coded = coded.replace(part1Key, "").replace(part2Key, "");

        String msg = new String(Base64.getDecoder().decode(coded));
        HashMap<String, Object> messageMap = MAPPER.readValue(msg, HashMap.class);
        String action = messageMap.get("action").toString();

        if (sessionMap == null) {
            sessionMap = new HashMap<>();
        }
        switch (action) {
            case "setEmailAddress":
                String emailAddress = messageMap.get("emailAddress").toString();
                User user = new User();
                user.setSession(session);
                user.setSessionId(session.getId());
                user.setEmailAddress(emailAddress);

                com.xendbit.xendserver.models.User debtor = UserDAO.findByColumn("email", emailAddress);

                HashMap<String, String> notificationRequest = new HashMap<>();

                sessionMap.put(emailAddress, user);

                if (debtor != null) {
                    settleDebts(debtor, notificationRequest);
                }

                notificationRequest.put("setEmailAddressResponse", "User Session Started Successfully");
                session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                break;
            case "startTrade":
                //buyer to seller
                //Check if trade can be bought                
                notificationRequest = new HashMap<>();
                String trxId = messageMap.get("trxId").toString();
                Exchange exchange = ExchangeDAO.findByTransactionId(trxId);

                if (!exchange.getStatus().equals(ExchangeDAO.STATUS.ORDER_PLACED.toString())) {
                    //The status is not order placed. Send a message to the seller
                    notificationRequest.put("action", "orderInProgress");
                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                } else {
                    String buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                    com.xendbit.xendserver.models.User buyer = UserDAO.findByColumn("email", buyerEmailAddress);

                    if (!buyer.isApproved()) {
                        notificationRequest.put("action", "accountNotApproved");
                        notificationRequest.put("message", "You account has not been approved by the Administrator. You can't buy coins on XendBit until your account has been approved");
                        sessionMap.get(buyerEmailAddress).getSession().getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                        break;
                    }

                    String buyerOtherAddress = messageMap.get("buyerOtherAddress").toString();

                    String buyerAddress = messageMap.get("buyerAddress").toString();

                    String sellerEmailAddress = exchange.getSeller().getEmailAddress();

                    User seller = sessionMap.get(sellerEmailAddress);

                    if (seller == null) {
                        notificationRequest.put("action", "notOnline");
                        session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                        break;
                    }

                    Session sellerSession = seller.getSession();

                    if (!sellerSession.isOpen()) {
                        notificationRequest.put("action", "notOnline");
                        session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                        break;
                    }

                    if (exchange.getStatus().equals(ExchangeDAO.STATUS.ORDER_PLACED.toString())) {
                        notificationRequest.put("trxId", trxId);
                        notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
                        notificationRequest.put("buyerAddress", buyerAddress);
                        notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
                        notificationRequest.put("action", "startTrade");
                        notificationRequest.put("amountToSell", "" + exchange.getAmountToSell());
                        notificationRequest.put("fromCoin", "" + exchange.getFromCoin());
                        notificationRequest.put("toCoin", "" + exchange.getToCoin());
                        notificationRequest.put("message", "A buyer is interested in buying your " + exchange.getFromCoin() + " @");

                        sellerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    } else {
                        notificationRequest = new HashMap<>();
                        notificationRequest.put("action", "cancelTrade");
                        session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    }
                }
                break;
            case "cancelBankPayment":
                trxId = messageMap.get("trxId").toString();
                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.ORDER_PLACED.toString());
            case "cancelTrade":
                //seller to buyer
                String buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                Session buyerSession = sessionMap.get(buyerEmailAddress).getSession();
                notificationRequest = new HashMap<>();
                notificationRequest.put("action", "cancelTrade");
                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                break;
            case "askBuyerToPay":
                //seller to buyer after startTrade
                //Check if trade can be bought
                notificationRequest = new HashMap<>();
                trxId = messageMap.get("trxId").toString();
                exchange = ExchangeDAO.findByTransactionId(trxId);
                String trxHex = messageMap.get("trxHex").toString();

                if (!exchange.getStatus().equals(ExchangeDAO.STATUS.ORDER_PLACED.toString())) {
                    //The status is not order placed. Send a message to the seller
                    notificationRequest.put("action", "orderInProgress");
                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                } else {
                    buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                    String buyerAddress = messageMap.get("buyerAddress").toString();
                    String buyerOtherAddress = messageMap.get("buyerOtherAddress").toString();
                    com.xendbit.xendserver.models.User buyer = UserDAO.findByColumn("email", buyerEmailAddress);

                    exchange.setBuyerId(buyer.getId());
                    exchange.setBuyerToAddress(buyerOtherAddress);
                    exchange.setBuyerFromAddress(buyerAddress);
                    exchange.setBuyerToAddress(buyerAddress);
                    exchange.setTrxHex(trxHex);
                    ExchangeDAO.updateBuyerInfo(exchange, ExchangeDAO.STATUS.SELLER_INTERESTED.toString());

                    String sellerOtherAddress = exchange.getSellerToAddress();

                    notificationRequest = new HashMap<>();
                    notificationRequest.put("trxId", trxId);
                    notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
                    notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
                    notificationRequest.put("sellerOtherAddress", sellerOtherAddress);
                    notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
                    notificationRequest.put("amountToSell", exchange.getAmountToSell() + "");
                    notificationRequest.put("toCoin", "" + exchange.getToCoin());
                    notificationRequest.put("seller", MAPPER.writeValueAsString(exchange.getSeller()));
                    notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
                    notificationRequest.put("action", "paySeller");

                    if (sessionMap.containsKey(buyerEmailAddress)) {
                        buyerSession = sessionMap.get(buyerEmailAddress).getSession();
                        if (buyerSession.isOpen()) {
                            buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                        }
                    }
                }
                break;
            case "sellerSentCoins":
                //seller to buyer
                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                com.xendbit.xendserver.models.User buyer = UserDAO.findByColumn("email", buyerEmailAddress);
                trxId = messageMap.get("trxId").toString();
                String buyerAddress = messageMap.get("buyerAddress").toString();
                String buyerOtherAddress = messageMap.get("buyerOtherAddress").toString();

                exchange = ExchangeDAO.findByTransactionId(trxId);
                String sellerOtherAddress = exchange.getSellerToAddress();

                notificationRequest = new HashMap<>();
                notificationRequest.put("trxId", trxId);
                notificationRequest.put("buyerEmailAddress", buyerEmailAddress);
                notificationRequest.put("buyerOtherAddress", buyerOtherAddress);
                notificationRequest.put("sellerOtherAddress", sellerOtherAddress);
                notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
                notificationRequest.put("toCoin", "" + exchange.getToCoin());
                notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
                notificationRequest.put("action", "sendCoinsToSeller");

                exchange.setBuyerId(buyer.getId());
                exchange.setBuyerToAddress(buyerOtherAddress);
                exchange.setBuyerFromAddress(buyerAddress);
                ExchangeDAO.updateBuyerInfo(exchange, ExchangeDAO.STATUS.SELLER_SENT_COINS.toString());
                if (sessionMap.containsKey(buyerEmailAddress)) {
                    buyerSession = sessionMap.get(buyerEmailAddress).getSession();
                    if (buyerSession.isOpen()) {
                        buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    }
                }
                break;
            case "buyerSentCoins":
                //buyer to seller
                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                trxId = messageMap.get("trxId").toString();
                exchange = ExchangeDAO.findByTransactionId(trxId);
                String sellerEmailAddress = exchange.getSeller().getEmailAddress();

                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.SUCCESS.toString());

                notificationRequest = new HashMap<>();
                notificationRequest.put("action", "success");

                buyerSession = sessionMap.get(buyerEmailAddress).getSession();
                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));

                if (sessionMap.containsKey(sellerEmailAddress)) {
                    Session sellerSession = sessionMap.get(sellerEmailAddress).getSession();
                    if (sellerSession.isOpen()) {
                        sellerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    }
                }
                break;
            case "buyerConfirmedBankPayment":
                //buyer to seller
                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                buyer = UserDAO.findByColumn("email", buyerEmailAddress);
                String buyerFullname = buyer.getFullname();
                trxId = messageMap.get("trxId").toString();
                exchange = ExchangeDAO.findByTransactionId(trxId);
                sellerEmailAddress = exchange.getSeller().getEmailAddress();
                com.xendbit.xendserver.models.User seller = UserDAO.findByColumn("email", sellerEmailAddress);
                
                notificationRequest = new HashMap<>();
                notificationRequest.put("action", "buyerConfirmedBankPayment");
                notificationRequest.put("buyerFullname", buyerFullname);
                notificationRequest.put("trxId", trxId);
                notificationRequest.put("amountToRecieve", exchange.getAmountToRecieve() + "");
                notificationRequest.put("fromCoin", exchange.getFromCoin());

                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.BUYER_PAID.toString());

                if (sessionMap.containsKey(sellerEmailAddress)) {
                    Session sellerSession = sessionMap.get(sellerEmailAddress).getSession();
                    if (sellerSession.isOpen()) {
                        sellerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    }
                }
                break;
            case "errorSendingToSeller":
                //buyer to seller
                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                trxId = messageMap.get("trxId").toString();
                exchange = ExchangeDAO.findByTransactionId(trxId);

                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.BUYER_SENDING_ERROR.toString());

                notificationRequest = new HashMap<>();
                notificationRequest.put("sellerOtherAddress", "" + exchange.getSellerToAddress());
                notificationRequest.put("coin", "" + exchange.getToCoin());
                notificationRequest.put("amount", "" + exchange.getAmountToRecieve());
                notificationRequest.put("action", "errorSendingToSeller");

                buyerSession = sessionMap.get(buyerEmailAddress).getSession();
                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                break;
            case "errorSendingToBuyer":
                //seller to buyer
                buyerEmailAddress = messageMap.get("buyerEmailAddress").toString();
                trxId = messageMap.get("trxId").toString();
                exchange = ExchangeDAO.findByTransactionId(trxId);

                ExchangeDAO.updateTradeStatus(trxId, ExchangeDAO.STATUS.BUYER_SENDING_ERROR.toString());

                notificationRequest = new HashMap<>();
                notificationRequest.put("action", "cancelTrade");

                buyerSession = sessionMap.get(buyerEmailAddress).getSession();
                buyerSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                break;
            case "askBeneficiaryForAddress":
                notificationRequest = new HashMap<>();
                String donorEmailAddress = messageMap.get("donorEmailAddress").toString();
                String beneficiaryEmailAddress = messageMap.get("beneficiaryEmailAddress").toString();

                User beneficiarySessionUser = sessionMap.get(beneficiaryEmailAddress);

                if (beneficiarySessionUser == null) {
                    notificationRequest.put("action", "notOnline");
                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    break;
                }

                Session beneficiarySession = beneficiarySessionUser.getSession();
                if (!beneficiarySession.isOpen()) {
                    notificationRequest.put("action", "notOnline");
                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    break;
                }

                com.xendbit.xendserver.models.User donor = UserDAO.findByColumn("email", donorEmailAddress);

                String donorFullname = donor.getFullname();

                notificationRequest.put("action", "provideAddressToDonor");
                notificationRequest.put("coin", messageMap.get("coin").toString());
                notificationRequest.put("donor", donorFullname);
                notificationRequest.put("donorEmailAddress", donorEmailAddress);
                beneficiarySession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                break;
            case "provideAddressToDonor":
                notificationRequest = new HashMap<>();
                donorEmailAddress = messageMap.get("donorEmailAddress").toString();
                User donorSessionUser = sessionMap.get(donorEmailAddress);

                if (donorSessionUser == null) {
                    notificationRequest.put("action", "notOnline");
                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    break;
                }

                Session donorSession = donorSessionUser.getSession();
                if (!donorSession.isOpen()) {
                    notificationRequest.put("action", "notOnline");
                    session.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));
                    break;
                }

                notificationRequest.put("action", "addressProvidedToDonor");
                notificationRequest.put("coin", messageMap.get("coin").toString());
                notificationRequest.put("address", messageMap.get("address").toString());
                donorSession.getBasicRemote().sendText(MAPPER.writeValueAsString(notificationRequest));

                break;
            default:
                LOGGER.info("Invalid Message Recieved");
                break;
        }
    }

    @OnMessage
    public void binaryMessage(Session session, ByteBuffer msg) {
        System.out.println("Binary message: " + msg.toString());
    }

    @OnMessage
    public void pongMessage(Session session, PongMessage msg) {
        System.out.println("Pong message: " + msg.getApplicationData().toString());
    }

    @OnError
    public void error(Session session, Throwable error) {
        LOGGER.log(Level.SEVERE, "Socket Error: " + error.getMessage(), error);
    }

    @OnClose
    public void close(Session session, CloseReason reason) {
        LOGGER.log(Level.SEVERE, "Socket Closed: {0} : {1}", new Object[]{reason.getReasonPhrase(), reason.getCloseCode().getCode()});
    }

    class User implements Serializable {

        String sessionId;
        String emailAddress;
        Session session;

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getEmailAddress() {
            return emailAddress;
        }

        public void setEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
        }

        public Session getSession() {
            return session;
        }

        public void setSession(Session session) {
            this.session = session;
        }
    }

    public static CloseableHttpClient registerHttps() {
        int timeout = 60000;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        CloseableHttpClient client = null;
        try {
            SSLContext sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, (certificate, authType) -> true).build();

            client = HttpClients.custom()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setDefaultRequestConfig(config)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }
}
