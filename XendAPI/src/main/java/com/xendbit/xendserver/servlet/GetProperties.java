/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.xendbit.xendserver.models.dao.PropertiesDAO;

/**
 *
 * @author aardvocate
 */
//@WebServlet(name = "GetProperties", urlPatterns = {"/en.ng.json"})
public class GetProperties extends HttpServlet {

    private static final long serialVersionUID = -8861587384804686622L;

    private static final ObjectMapper OM = new ObjectMapper();
    
    public static final String PAYMENT_METHODS_TEXT   =   "payment.methods";
    public static final String ID_TYPES_TEXT          =   "id.types";
    public static final String BANKS_TEXT             =   "banks";
    public static final String EQUITIES_TEXT          =   "equities";
    public static final String WALLETS_TEXT           =   "wallets";
    public static final String XEND_BANKS_TEXT        =   "xend.banks";
    
    public static final int     PAYMENT_METHODS_ID   =   100;
    public static final int     ID_TYPES_ID          =   200;
    public static final int     BANKS_ID             =   300;
    public static final int     EQUITIES_ID          =   400;
    public static final int     WALLETS_ID           =   500;
    public static final int     XEND_BANKS_ID        =   600;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        

        HashMap<String, Object> result = new HashMap<>();
        result.put(PAYMENT_METHODS_TEXT, PropertiesDAO.getProperties(PAYMENT_METHODS_ID));               
        result.put(ID_TYPES_TEXT, PropertiesDAO.getProperties(ID_TYPES_ID));                        
        result.put(BANKS_TEXT, PropertiesDAO.getProperties(BANKS_ID));                        
        result.put(EQUITIES_TEXT, PropertiesDAO.getProperties(EQUITIES_ID));                       
        result.put(WALLETS_TEXT, PropertiesDAO.getProperties(WALLETS_ID));     
        result.put(XEND_BANKS_TEXT, PropertiesDAO.getProperties(XEND_BANKS_ID));
                        
        response.getWriter().write(OM.writeValueAsString(result));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
