package com.xendbit.xendserver.servlet;


import com.xendbit.xendserver.api.utils.WalletKitHelper;
import com.xendbit.xendserver.models.User;

import com.xendbit.xendserver.models.dao.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Logger;


/**
 * Created by aardvocate on 7/31/17.
 */
public class GetImage extends HttpServlet {
    public static final long serialVersionUID = UUID.randomUUID().getMostSignificantBits();
    protected static final Logger LOGGER = Logger.getLogger(GetImage.class.getName());


    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException {
        new WalletKitHelper();
        response.setContentType("image/jpg");
        PrintWriter out = response.getWriter();
        String md5Email = request.getParameter("c");
        User user = UserDAO.findByEmailMD5(md5Email);
        if (user == null) {
            returnUserNotFoundResponse(out);
        } else if (user.isActivated()) {
            returnUserNotFoundResponse(out);
        } else {
            UserDAO.activateAccount(user);
            returnUserFoundResponse(out, user);
        }

        out.flush();
        out.close();
    }

    private void returnUserFoundResponse(PrintWriter out, User user) {
        String html = "<div style='font-size: 20px; color: #000;'>" +
                "    <p>Dear " + user.getFullname() + "</p>" +
                "    Welcome to Xendbit. Your email validation is successful. Please proceed to the app to login.<br /><br />" +
                "    <hr>" +
                "    &copy; 2017 <small>Xendbit</small>" +
                "  </div>";
        out.println(html);
    }

    private void returnUserNotFoundResponse(PrintWriter out) {
        String html = "<div style='font-size: 20px; color: #000;'>" +
                "    Confirmation link is invalid.<br /><br />" +
                "    <hr>" +
                "    &copy; 2017 <small>Xendbit</small>" +
                "  </div>";
        out.println(html);
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        try {
            processRequest(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
