package com.xendbit.xendserver.api;

import com.xendbit.xendserver.api.utils.AppMessenger;
import com.xendbit.xendserver.api.utils.BlockchainHelper;
import com.xendbit.xendserver.api.utils.DefaultAppMessenger;
import com.xendbit.xendserver.api.utils.MoneyWaveAPI;
import com.xendbit.xendserver.api.utils.TransactionsGetter;
import com.xendbit.xendserver.api.utils.WalletKitHelper;
import com.xendbit.xendserver.api.utils.XendException;
import com.xendbit.xendserver.models.BuyRequest;
import com.xendbit.xendserver.models.Exchange;
import com.xendbit.xendserver.models.SendObject;
import com.xendbit.xendserver.models.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import com.xendbit.xendserver.models.Transaction;
import com.xendbit.xendserver.models.dao.APIUserDAO;
import com.xendbit.xendserver.models.dao.BuyRequestDAO;
import com.xendbit.xendserver.models.dao.ExchangeDAO;
import com.xendbit.xendserver.models.dao.UserDAO;
import com.xendbit.xendserver.models.utils.BCrypt;
import java.io.IOException;

@Path("exchange")
public class ExchangeAPI {

    protected static final Logger LOGGER = Logger.getLogger(ExchangeAPI.class.getName());

//    TODO: Write the API to return pending, it is the transaction in pending that must be signed and pushed to the blockchain
//    After that is done, then update the status of XB_PENDING and XB_EXCHANGE to SUCCESS
    @Path("{trxId}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getExchange(@HeaderParam("apiKey") String apiKey, @PathParam("trxId") String trxId) {
        LOGGER.info("Called getExchange");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        res.put("result", ExchangeDAO.findByTransactionId(trxId));
        return Response.ok(res).build();
    }

    @Path("how-much-can-we-sell/{ticker}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response howMuchCanWeSell(@HeaderParam("apiKey") String apiKey, @PathParam("ticker") String ticker) throws Exception {
        LOGGER.info("Called howMuchCanWeSell");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        res.put("result", WalletKitHelper.howMuchCanWeSell(ticker));
        return Response.ok(res).build();
    }

    @Path("usdrate/{ticker}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getExchangeRate(@HeaderParam("apiKey") String apiKey, @PathParam("ticker") String ticker) throws IOException {
        LOGGER.info("Called getExchangeRate");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }
        //Assume USD is the base currency
        double buyRate = Double.parseDouble(WalletKitHelper.p.getProperty("usd.ngn.buy.xrate"));
        double sellRate = Double.parseDouble(WalletKitHelper.p.getProperty("usd.ngn.sell.xrate"));

        HashMap<String, Double> xRates = new HashMap<>();
        xRates.put("buy", buyRate);
        xRates.put("sell", sellRate);
        xRates.put("rate", BlockchainHelper.getUsdValue(ticker));
        res.put("result", xRates);
        return Response.ok(res).build();
    }

    @Path("xrate/{ticker1}/{ticker2}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getExchangeRate(@HeaderParam("apiKey") String apiKey, @PathParam("ticker1") String ticker1, @PathParam("ticker2") String ticker2) throws IOException {
        LOGGER.info("Called getExchangeRate");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        double ticker1RateUsd = BlockchainHelper.getUsdValue(ticker1);//7977.72
        double ticker2RateUsd = BlockchainHelper.getUsdValue(ticker2);//145.144
        double t1ToT2ExchangeRate = ticker1RateUsd / ticker2RateUsd;
        double t2ToT1ExchangeRate = ticker2RateUsd / ticker1RateUsd;

        HashMap<String, Double> xRates = new HashMap<>();
        xRates.put("t1_t2", t1ToT2ExchangeRate);
        xRates.put("t2_t1", t2ToT1ExchangeRate);
        res.put("result", xRates);
        return Response.ok(res).build();
    }

    @Path("buy-direct")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response buyDirect(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else if (!dbUser.isApproved()) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Your account is awaiting approval. You can not use this feature until your account is approved.");
        } else {
            dbUser.setWalletCode(sendObject.getFromCoin());
            dbUser.setNetworkAddress(sendObject.getNetworkAddress());
            dbUser.setWalletCode(wallet);
            HashMap<String, Object> localRequestData = new HashMap<>();
            localRequestData.put("send-object", sendObject);
            localRequestData.put("user", dbUser);

            AppMessenger appMessenger = new DefaultAppMessenger(localRequestData);

            boolean buyOrderResult = WalletKitHelper.buyDirect(appMessenger);
            res.put("response_text", buyOrderResult ? "success" : "error");
            res.put("result", buyOrderResult ? "Order successfully placed." : "Error Placing Order");
        }

        return Response.ok(res).build();
    }

    @Path("post-trade")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response postTrade(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else if (!dbUser.isApproved()) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Your account is awaiting approval. You can not use this feature until your account is approved.");
        } else {
            dbUser.setWalletCode(sendObject.getFromCoin());
            dbUser.setNetworkAddress(sendObject.getNetworkAddress());
            dbUser.setWalletCode(wallet);
            dbUser.setEquityId(sendObject.getEquityId());

            if (checkBalance(dbUser, sendObject) || sendObject.isSellerIsAdmin()) {
                HashMap<String, Object> localRequestData = new HashMap<>();
                localRequestData.put("send-object", sendObject);
                localRequestData.put("user", dbUser);

                AppMessenger appMessenger = new DefaultAppMessenger(localRequestData);

                boolean sellOrderResult = WalletKitHelper.addNewTrade(appMessenger);
                res.put("response_text", sellOrderResult ? "success" : "error");
                res.put("result", sellOrderResult ? "Order successfully placed." : "Error Placing Order");
            } else {
                res.put("response_code", 0);
                res.put("response_text", "error");
                res.put("result", "Insufficient " + sendObject.getFromCoin() + " Balance");
            }
        }
        return Response.ok(res).build();
    }

    @Path("sell-orders")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @POST
    public Response getSellOrders(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        LOGGER.info("getSellers");
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            res.put("response_code", 0);
            res.put("response_text", "success");

            dbUser.setWalletCode(wallet);
            List<Exchange> exchanges = ExchangeDAO.getTrades(dbUser);
            LOGGER.info(exchanges.toString());

            List<Exchange> filtered = exchanges.stream().filter(x -> ExchangeDAO.STATUS.ORDER_PLACED.toString().equals(x.getStatus())).collect(Collectors.toList());

            res.put("result", filtered);
        }
        return Response.ok(res).build();
    }

    /**
     * Get buy orders placed by the requesting user
     *
     * @param sendObject Object containing the API parameters {user.email,
     * @param apiKey 
     * @param wallet 
     * user.password}.
     * @return a list of buy orders placed by the requesting user or an empty
     * list
     */
    @Path("my-buy-orders")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @POST
    public Response getUserBuyOrders(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        LOGGER.info("getUserBuyOrders");
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            res.put("response_code", 0);
            res.put("response_text", "success");

            dbUser.setWalletCode(wallet);
            List<BuyRequest> buyRequests = BuyRequestDAO.getUserBuyOrders(dbUser);

            res.put("result", buyRequests);
        }
        return Response.ok(res).build();
    }

    /**
     * Get sell orders placed by the requesting user
     *
     * @param sendObject Object containing the API parameters {user.email,
     * user.password}.
     * @param apiKey 
     * @param wallet 
     * @return a list of sell orders placed by the requesting user or an empty
     * list
     */
    @Path("my-sell-orders")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @POST
    public Response getUserSellerOrders(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        LOGGER.info("getUserSellOrders");
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            res.put("response_code", 0);
            res.put("response_text", "success");

            dbUser.setWalletCode(wallet);
            List<Exchange> exchanges = ExchangeDAO.getUserSellOrders(dbUser);

            res.put("result", exchanges);
        }
        return Response.ok(res).build();
    }

    /**
     * Delete sell orders placed by the requesting user.
     *
     * @param sendObject Object containing the API parameters {user.email,
     * user.password}.
     * @return status of the delete (success | error) and the number of items
     * deleted.Should be typically 1
     */
    @Path("my-sell-orders/update")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @POST
    public Response deleteUserSellOrders(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        LOGGER.info("deleteUserSellOrders");
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            res.put("response_code", 0);
            res.put("response_text", "success");

            boolean updated = false;
            dbUser.setWalletCode(wallet);
            if (sendObject.getStatus().equals("delete")) {
                String trxId = sendObject.getSellOrderTransactionId();
                updated = ExchangeDAO.ownerCancelled(trxId, ExchangeDAO.STATUS.OWNER_CANCELED.toString());
            }

            res.put("result", updated);
        }
        return Response.ok(res).build();
    }

    @Path("utxos/{address}")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUTXOs(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, @PathParam("address") String address, SendObject sendObject) {
        LOGGER.info("Called getUTXOs");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", -1);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        List<Transaction> utxos = new ArrayList<>();
        List<Transaction> unspentOutputs = TransactionsGetter.getUnspentOutputs(address, wallet);

        double cummulativeValue = 0;
        double amountDouble = Double.parseDouble(sendObject.getBtcValue());
        if (unspentOutputs != null) {
            for (Transaction utxo : unspentOutputs) {
                double value = utxo.getValue();
                cummulativeValue += value;
                utxos.add(utxo);
                if (amountDouble <= cummulativeValue) {
                    break;
                }
            }
        }

        if (cummulativeValue < amountDouble) {
            res.put("response_code", -1);
            res.put("response_text", "error");
            res.put("result", "Wallet balance is too low for this transaction + fees. If you have a transaction that is pending confirmation, please wait for the transaction to have at least one confirmation and try again");
        } else {
            res.put("result", utxos);
        }
        return Response.ok(res).build();
    }

    /**
     * Verify that bank account exists
     *
     * @param apiKey
     * @param sendObject
     * @return the name on the bank account
     */
    @Path("account/verify")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response verifyBankAccount(@HeaderParam("apiKey") String apiKey, SendObject sendObject) {
        LOGGER.info("Called verifyBankAccount");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        try {
            res.put("account_name", MoneyWaveAPI.verifyBankAccount(sendObject));
        } catch (XendException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error: [" + e.getMessage() + "]");
        }

        return Response.ok(res).build();
    }

    private boolean checkBalance(User user, SendObject sendObject) {
        user.setCurrencyId(sendObject.getCurrencyId());
        HashMap<String, Object> userDisplayInfo = TransactionsGetter.getTransactions(user, -1, -1);
        double balance = Double.parseDouble(userDisplayInfo.get("balance").toString());
        if (sendObject.getAmountToSell() > balance) {
            return false;
        }

        return true;
    }

    /**
     * Push a transaction to the blockchain
     *
     * @param wallet
     * @param apiKey
     * @param sendObject Object containing the API parameters {transaction hex}.
     * @return if transaction is pushed or not
     */
    @Path("pushtx")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @POST
    public Response pushTx(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        LOGGER.info("pushTx");
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }

        if (sendObject.getEmailAddress().contains("guest-") && sendObject.getPassword().equals("guest")) {
            dbUser = new User();
            dbUser.setEmailAddress("guest");
            dbUser.setId(0l);
        }

        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            res.put("response_code", 0);
            res.put("response_text", "success");

            res.put("result", BlockchainHelper.pushTx(sendObject, wallet));
        }
        return Response.ok(res).build();
    }

    @Path("market-data")
    @Produces({MediaType.APPLICATION_JSON})
    @GET
    public Response getMarketData() {
        return Response.ok(BlockchainHelper.getMarketData()).build();
    }

    @Path("update-exchange-status")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @POST
    public Response updateTradeStatus(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, SendObject sendObject) {
        LOGGER.info("pushTx");
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", sendObject.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            res.put("response_code", 0);
            res.put("response_text", "success");

            LOGGER.info(sendObject.getSellOrderTransactionId());
            LOGGER.info(sendObject.getStatus());

            ExchangeDAO.updateTradeStatus(sendObject.getSellOrderTransactionId(), sendObject.getStatus());
        }
        return Response.ok(res).build();
    }

    //get all transactions by date limit by page
    @Path("{startDate}/{endDate}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransactionsByDateLimitByPage(@HeaderParam("apiKey") String apiKey, @PathParam("startDate") long startDate, @PathParam("endDate") long endDate) {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        try {
            List<Exchange> transactions = ExchangeDAO.getTransactionsByDate(startDate, endDate);
            if (transactions != null) {
                res.put("result", transactions);
            } else {
                res.put("response_code", -3);
                res.put("response_text", "success");
                res.put("result", "There are no transactions currently existing");
            }
        } catch (Exception e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error gettting transactions [" + e.getMessage() + "]");
            e.printStackTrace();
        }
        return Response.ok(res).build();

    }

}
