package com.xendbit.xendserver.api.utils;

import com.xendbit.xendserver.models.SendObject;
import com.xendbit.xendserver.models.User;
import edu.self.kraken.api.KrakenApi;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unchecked")
public class BlockchainHelper {

    private static final Logger LOGGER = Logger.getLogger(BlockchainHelper.class.getName());

    public static HashMap<String, Object> pushTx(SendObject sendObject, String walletCode) {

        if (sendObject.getCurrencyId() != null && sendObject.getCurrencyId().length() > 0) {
            //try and send the fees.
            //TODO: If we can't take the fees, what do we do???
            HashMap response = pushXndTokenTx(sendObject);
            pushXndTx(getXendFeesObject(sendObject));
            return response;
        }

        if (sendObject.getEquityId() != null && sendObject.getEquityId().length() > 0) {
            //try and send the fees.
            //TODO: If we can't take the fees, what do we do???
            return transferAsset(sendObject);
        }

        User.WALLET_CODES wc = User.WALLET_CODES.valueOf(walletCode);
        switch (wc) {
            case BTC:
            case BTCTEST:
            case DASH:
            case DASHTEST:
            case LTC:
            case LTCTEST:
                return pushBitcoinTx(sendObject, walletCode);
            case XND:
                //try and send the fees.
                //TODO: If we can't take the fees, what do we do???
                HashMap response = pushXndTx(sendObject);
                pushXndTx(getXendFeesObject(sendObject));
                return response;
            case NXT:
                //try and send the fees.
                //TODO: If we can't take the fees, what do we do???
                response = pushNxtTx(sendObject);
                pushNxtTx(getXendFeesObject(sendObject));
                return response;
            case ARDOR:
                //try and send the fees.
                //TODO: If we can't take the fees, what do we do???
                response = pushARDRTx(sendObject, 1);
                pushARDRTx(getXendFeesObject(sendObject), 1);
                return response;
            case IGNIS:
                //try and send the fees.
                //TODO: If we can't take the fees, what do we do???
                response = pushARDRTx(sendObject, 2);
                pushARDRTx(getXendFeesObject(sendObject), 2);
                return response;
            default:
                break;
        }
        return null;
    }

    private static HashMap<String, Object> getBlockchainStatus() {
        try {
            String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
            String transactionsUrl = BASE_URL + "requestType=getBlockchainStatus";
            LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);
            CloseableHttpClient client = WalletKitHelper.registerHttps();
            HttpGet getRequest = new HttpGet(transactionsUrl);
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());

            return response;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    private static HashMap<String, Object> transferAsset(SendObject sendObject) {
        HashMap<String, Object> blockStatus = getBlockchainStatus();
        String blockHeight = "100000";
        if (blockStatus != null && blockStatus.containsKey("numberOfBlocks")) {
            blockHeight = (Long.parseLong(getBlockchainStatus().get("numberOfBlocks").toString()) + 100) + "";
        }

        try {
            String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
            String transactionsUrl = BASE_URL + "requestType=transferAsset";

            LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);
            CloseableHttpClient client = WalletKitHelper.registerHttps();
            HttpPost postRequest = new HttpPost(transactionsUrl);

            List<NameValuePair> fields = new ArrayList<>();

            fields.add(new BasicNameValuePair("recipient", sendObject.getToAddress().replaceAll("XND", "NXT")));
            fields.add(new BasicNameValuePair("asset", sendObject.getEquityId()));
            fields.add(new BasicNameValuePair("quantityQNT", sendObject.getBtcValue()));
            fields.add(new BasicNameValuePair("secretPhrase", sendObject.getPassphrase()));
            fields.add(new BasicNameValuePair("feeNQT", (2 * WalletKitHelper.ONE_NXT) + ""));
            fields.add(new BasicNameValuePair("deadline", "6"));
            if (sendObject.getBrokerAccount() != null) {
                fields.add(new BasicNameValuePair("phased", "true"));
                fields.add(new BasicNameValuePair("phasingVotingModel", "0"));
                fields.add(new BasicNameValuePair("phasingQuorum", "1"));
                fields.add(new BasicNameValuePair("phasingWhitelisted", sendObject.getBrokerAccount().replaceAll("XND", "NXT")));
                fields.add(new BasicNameValuePair("phasingFinishHeight", blockHeight));
            }

            //Content-Type: application/x-www-form-urlencoded; charset=UTF-8
            postRequest.setEntity(new UrlEncodedFormEntity(fields));

            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());

            return response;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    private static HashMap<String, Object> pushXndTokenTx(SendObject sendObject) {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
        String transactionsUrl = BASE_URL + "requestType=transferCurrency";

        LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpPost postRequest = new HttpPost(transactionsUrl);

        List<NameValuePair> fields = new ArrayList<>();

        fields.add(new BasicNameValuePair("recipient", sendObject.getToAddress().replaceAll("XND", "NXT")));
        fields.add(new BasicNameValuePair("currency", sendObject.getCurrencyId()));
        fields.add(new BasicNameValuePair("units", sendObject.getBtcValue()));
        fields.add(new BasicNameValuePair("secretPhrase", sendObject.getPassphrase()));
        fields.add(new BasicNameValuePair("feeNQT", WalletKitHelper.ONE_NXT + ""));
        fields.add(new BasicNameValuePair("deadline", "6"));

        //Content-Type: application/x-www-form-urlencoded; charset=UTF-8
        try {
            postRequest.setEntity(new UrlEncodedFormEntity(fields));

            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());

            return response;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    private static SendObject getXendFeesObject(SendObject sendObject) {
        SendObject feesObject = new SendObject();
        feesObject.setToAddress(sendObject.getXendAddress());
        feesObject.setBtcValue(sendObject.getXendFees());
        feesObject.setPassphrase(sendObject.getPassphrase());

        return feesObject;
    }

    private static HashMap<String, Object> pushXndTx(SendObject sendObject) {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");

        String transactionsUrl = BASE_URL + "requestType=sendMoney";

        LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpPost postRequest = new HttpPost(transactionsUrl);

        List<NameValuePair> fields = new ArrayList<>();

        fields.add(new BasicNameValuePair("recipient", sendObject.getToAddress().replaceAll("XND", "NXT")));
        fields.add(new BasicNameValuePair("amountNQT", sendObject.getBtcValue()));
        fields.add(new BasicNameValuePair("secretPhrase", sendObject.getPassphrase()));
        fields.add(new BasicNameValuePair("feeNQT", WalletKitHelper.ONE_NXT + ""));
        fields.add(new BasicNameValuePair("deadline", "6"));
        LOGGER.log(Level.INFO, "Recipient: {0}", sendObject.getToAddress());
        LOGGER.log(Level.INFO, "Amount: {0}", sendObject.getBtcValue());
        //Content-Type: application/x-www-form-urlencoded; charset=UTF-8
        try {
            postRequest.setEntity(new UrlEncodedFormEntity(fields));

            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
            return response;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    private static HashMap<String, Object> pushNxtTx(SendObject sendObject) {
        String BASE_URL = WalletKitHelper.p.getProperty("nxt.api.url");

        String transactionsUrl = BASE_URL + "requestType=sendMoney";

        LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpPost postRequest = new HttpPost(transactionsUrl);

        List<NameValuePair> fields = new ArrayList<>();

        fields.add(new BasicNameValuePair("recipient", sendObject.getToAddress()));
        fields.add(new BasicNameValuePair("amountNQT", sendObject.getBtcValue()));
        fields.add(new BasicNameValuePair("secretPhrase", sendObject.getPassphrase()));
        fields.add(new BasicNameValuePair("feeNQT", WalletKitHelper.ONE_NXT + ""));
        fields.add(new BasicNameValuePair("deadline", "6"));
        //Content-Type: application/x-www-form-urlencoded; charset=UTF-8
        try {
            postRequest.setEntity(new UrlEncodedFormEntity(fields));

            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
            return response;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    /**
     *
     * @param sendObject
     * @param chain ARDR = 1, IGNIS = 2
     * @return
     */
    private static HashMap<String, Object> pushARDRTx(SendObject sendObject, int chain) {
        String BASE_URL = WalletKitHelper.p.getProperty("ardr.api.url");

        String transactionsUrl = BASE_URL + "requestType=sendMoney&chain=" + chain;

        LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpPost postRequest = new HttpPost(transactionsUrl);

        List<NameValuePair> fields = new ArrayList<>();

        long fee = WalletKitHelper.ONE_NXT * 2;
        if (chain == 2) {
            fee = WalletKitHelper.ONE_NXT / 1000;
        }

        fields.add(new BasicNameValuePair("recipient", sendObject.getToAddress()));
        fields.add(new BasicNameValuePair("amountNQT", sendObject.getBtcValue()));
        fields.add(new BasicNameValuePair("secretPhrase", sendObject.getPassphrase()));
        fields.add(new BasicNameValuePair("feeNQT", fee + ""));
        fields.add(new BasicNameValuePair("deadline", "6"));
        fields.add(new BasicNameValuePair("chain", chain + ""));
        //Content-Type: application/x-www-form-urlencoded; charset=UTF-8
        try {
            postRequest.setEntity(new UrlEncodedFormEntity(fields));

            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
            return response;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    public static HashMap<String, Object> pushBitcoinTx(SendObject sendObject, String wallet) {
        String pushTxUrl = WalletKitHelper.p.getProperty("pushtx.url") + "/" + wallet;
        LOGGER.info(pushTxUrl);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpPost postRequest = new HttpPost(pushTxUrl);
        postRequest.setHeader("content-type", "application/json");
        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("apiKey", "oalkuisnetgauyno");

        HashMap<String, Object> fields = new HashMap<>();
        fields.put("transactionHex", sendObject.getTransactionHex());

        try {
            String jsonString = WalletKitHelper.OM.writeValueAsString(fields);
            StringEntity entity = new StringEntity(jsonString);
            entity.setContentType("application/json");

            postRequest.setEntity(entity);

            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
            return response;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    public static double howMuchCanWeSell(String ticker) throws Exception {
        ticker = ticker.toUpperCase();
        ticker = ticker.equals("BTC") ? "XBT" : ticker;

        String response;
        Map<String, String> input = new HashMap<>();

        String pairName = "X" + ticker + "ZEUR";
        input.put("pair", ticker + "EUR");
        response = WalletKitHelper.krakenApi.queryPublic(KrakenApi.Method.TICKER, input);
        LOGGER.log(Level.INFO, "TICKER RESPONSE: {0}", response);
        HashMap<String, Object> result = WalletKitHelper.OM.readValue(response, HashMap.class);
        HashMap resultMap = (HashMap) result.get("result");
        HashMap pairMap = (HashMap) resultMap.get(pairName);
        ArrayList highList = (ArrayList) pairMap.get("h");
        double high = Double.parseDouble(highList.get(0).toString());

        input.clear();
        input.put("asset", "EUR");
        response = WalletKitHelper.krakenApi.queryPrivate(KrakenApi.Method.BALANCE, input);
        result = WalletKitHelper.OM.readValue(response, HashMap.class);
        LOGGER.log(Level.INFO, "BALANCE RESPONSE: {0}", response);
        resultMap = (HashMap) result.get("result");
        double usdBalance = Double.parseDouble(resultMap.get("ZEUR").toString()) + 5000;

        double howMuchCanWeSell = usdBalance / high;

        return howMuchCanWeSell;
    }

    public static double getUsdValue(String ticker) throws IOException {
        double usdValue = 0.0;
        try {
            usdValue = Double.parseDouble(WalletKitHelper.p.getProperty(ticker + ".usd.value"));
        } catch (Exception e) {

        }

        if (usdValue != 0) {
            return usdValue;
        }

        if (ticker.equalsIgnoreCase("BTC")) {
            ticker = ticker.toUpperCase();
            ticker = ticker.equals("BTC") ? "XBT" : ticker;

            String response;
            Map<String, String> input = new HashMap<>();

            String pairName = "X" + ticker + "ZUSD";
            input.put("pair", ticker + "USD");
            response = WalletKitHelper.krakenApi.queryPublic(KrakenApi.Method.TICKER, input);
            LOGGER.log(Level.INFO, "Kraken Reponse: {0}", response);
            HashMap<String, Object> result = WalletKitHelper.OM.readValue(response, HashMap.class);
            HashMap resultMap = (HashMap) result.get("result");
            HashMap pairMap = (HashMap) resultMap.get(pairName);
            ArrayList priceList = (ArrayList) pairMap.get("c");
            double price = Double.parseDouble(priceList.get(0).toString());
            double markupRate = Double.parseDouble(WalletKitHelper.p.getProperty("xend.markup")) / 100.0;
            double highPlus3Percent = price + (markupRate * price);
            return highPlus3Percent;
        } else {
          try {
            HashMap<String, Object> cmcData = CMCUtils.getPrice(ticker.toUpperCase(), "USD", "1");
            double price = Double.parseDouble(cmcData.get("price").toString());
            double markupRate = Double.parseDouble(WalletKitHelper.p.getProperty("xend.markup")) / 100.0;
            double highPlus3Percent = price + (markupRate * price);
            return highPlus3Percent;
          } catch (Exception e1) {
            e1.printStackTrace();
          }
        }

        return 0;
    }

    public static String getXndAccountId(String secretPhrase) throws UnsupportedEncodingException {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
        String url = BASE_URL + "requestType=getAccountId&secretPhrase=" + URLEncoder.encode(secretPhrase, "UTF-8");
        LOGGER.info(url);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(url);

        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8").replaceAll("NXT", "XND");
            LOGGER.info(s);
            return s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "failed";
    }

    public static String getNxtAccountId(String secretPhrase) throws UnsupportedEncodingException {
        String BASE_URL = WalletKitHelper.p.getProperty("nxt.api.url");
        String url = BASE_URL + "requestType=getAccountId&secretPhrase=" + URLEncoder.encode(secretPhrase, "UTF-8");
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(url);

        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            return s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "failed";
    }

    /**
     *
     * @param secretPhrase
     * @param wallet
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getARDRAccountId(String secretPhrase, String wallet) throws UnsupportedEncodingException {

        int chain = 1;
        if (wallet.equals(User.WALLET_CODES.IGNIS.name())) {
            chain = 2;
        }
        String BASE_URL = WalletKitHelper.p.getProperty("ardr.api.url");
        String url = BASE_URL + "requestType=getAccountId&chain=" + chain + "&secretPhrase=" + URLEncoder.encode(secretPhrase, "UTF-8");
        LOGGER.info(url);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(url);

        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(url);
            return s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "failed";
    }

    public static List<HashMap<String, Object>> getAllCurrencies() {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
        String url = BASE_URL + "requestType=getAllCurrencies";
        LOGGER.info(url);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(url);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();

            HashMap<String, Object> currenciesResult = WalletKitHelper.OM.readValue(is, HashMap.class);
            if (currenciesResult.containsKey("currencies")) {
                List<HashMap<String, Object>> currencies = (List<HashMap<String, Object>>) currenciesResult.get("currencies");
                return currencies;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static HashMap<String, Object> getXndAccountCurrencies(String networkAddress, User user) throws UnsupportedEncodingException {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
        String url = BASE_URL + "requestType=getAccountCurrencies&account=" + URLEncoder.encode(networkAddress.replaceAll("XND", "NXT"), "UTF-8") + "&includeCurrencyInfo=true&currency=" + user.getCurrencyId();
        LOGGER.info(url);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(url);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();

            HashMap<String, Object> accountCurrencies = WalletKitHelper.OM.readValue(is, HashMap.class);
            return accountCurrencies;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    public static HashMap<String, Object> getAccountEquities(String networkAddress, String equityId) throws UnsupportedEncodingException {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
        String url = BASE_URL + "requestType=getAccountAssets&account=" + URLEncoder.encode(networkAddress.replaceAll("XND", "NXT"), "UTF-8") + "&asset=" + equityId + "&includeAssetInfo=true";
        LOGGER.info(url);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(url);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();

            HashMap<String, Object> accountCurrencies = WalletKitHelper.OM.readValue(is, HashMap.class);
            return accountCurrencies;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    public static HashMap<String, Object> getMarketData() {
        String tickerUrl = WalletKitHelper.p.getProperty("ticker.api.url");
        String globalDataUrl = tickerUrl.replace("ticker", "global");

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(tickerUrl);
        getRequest.setHeader("content-type", "application/json");

        HashMap<String, Object> result = new HashMap<>();

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();

            ArrayList<HashMap<String, Object>> tickerResult = WalletKitHelper.OM.readValue(is, ArrayList.class);

            getRequest = new HttpGet(globalDataUrl);
            getRequest.setHeader("content-type", "application/json");
            httpResponse = client.execute(getRequest);
            is = httpResponse.getEntity().getContent();
            HashMap<String, Object> globalDataResult = WalletKitHelper.OM.readValue(is, HashMap.class);

            ArrayList<HashMap<String, Object>> tickers = new ArrayList<>();
            tickerResult.stream().forEach(x -> {
                HashMap<String, Object> aTicker = new HashMap<>();
                aTicker.put("name", x.get("name"));
                aTicker.put("price_usd", x.get("price_usd"));

                tickers.add(aTicker);
            });
            result.put("tickers", tickers);
            result.put("globalData", globalDataResult);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return result;
    }

    public static HashMap<String, Object> getAccountKYC(User u) {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");
        String url = BASE_URL + "requestType=getAccount&account=" + u.getXendNetworkAddress().replaceAll("XND", "NXT");
        LOGGER.info(url);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(url);

        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8").replaceAll("NXT", "XND");
            return WalletKitHelper.OM.readValue(s, HashMap.class);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return new HashMap<>();
    }
}
