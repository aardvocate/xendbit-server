package com.xendbit.xendserver.api;

import com.xendbit.xendserver.api.utils.AppMessenger;
import com.xendbit.xendserver.api.utils.BlockchainHelper;
import com.xendbit.xendserver.api.utils.DefaultAppMessenger;
import com.xendbit.xendserver.api.utils.WalletKitHelper;
import static com.xendbit.xendserver.api.utils.WalletKitHelper.OM;
import com.xendbit.xendserver.models.Group;
import com.xendbit.xendserver.models.SendObject;
import com.xendbit.xendserver.models.User;
import static com.xendbit.xendserver.models.User.WALLET_CODES.NXT;
import static com.xendbit.xendserver.models.User.WALLET_CODES.XND;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import com.xendbit.xendserver.models.dao.APIUserDAO;
import com.xendbit.xendserver.models.dao.AdminDAO;
import com.xendbit.xendserver.models.dao.UserDAO;
import com.xendbit.xendserver.models.utils.BCrypt;
import org.apache.commons.mail.EmailException;

@Path("user")
@SuppressWarnings("unchecked")
public class UserAPI {

    protected static final Logger LOGGER = Logger.getLogger(UserAPI.class.getName());

    @Path("send-2fa")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response get2FACode(@HeaderParam("apiKey") String apiKey, User requestUser) {
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }
        res.put("response_code", 0);
        res.put("response_text", "success");
        res.put("result", WalletKitHelper.get2FACode(requestUser));
        return Response.ok(res).build();
    }

    @Path("get-last-word")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response get13thWord(@HeaderParam("apiKey") String apiKey, User requestUser)
            throws IOException, NoSuchAlgorithmException {
        Map<String, Object> res = new HashMap<>();
        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }
        res.put("response_code", 0);
        res.put("response_text", "success");
        res.put("result", calculateChecksum(requestUser.getPassphrase()));
        return Response.ok(res).build();
    }

    public String calculateChecksum(String passphrase) throws NoSuchAlgorithmException {
        String[] splitted = passphrase.split(" ");
        StringBuilder trimmed = new StringBuilder();
        for (String word : splitted) {
            trimmed.append(word.substring(0, 2));
        }

        String trimmedString = trimmed.toString();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(trimmedString.getBytes(StandardCharsets.UTF_8));

        Checksum checksum = new CRC32();

        for (int i = 0; i < 11; i++) {
            String encodedString = bytesToHex(encodedhash);
            encodedhash = digest.digest(encodedString.getBytes(StandardCharsets.UTF_8));
        }

        String encodedString = bytesToHex(encodedhash);
        checksum.update(encodedString.getBytes(), 0, encodedString.length());

        long index = checksum.getValue() % 12;
        return splitted[(int) index];
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    @Path("get-account-id/{chain}")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getXndAccountId(@HeaderParam("apiKey") String apiKey, @PathParam("chain") String chain,
            SendObject sendObject) throws IOException {
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        LOGGER.log(Level.INFO, "{0} {1}", new Object[]{chain, chain.equals(XND.name())});
        if (chain.equals(XND.name())) {
            String s = BlockchainHelper.getXndAccountId(sendObject.getPassphrase());
            HashMap<String, Object> result = WalletKitHelper.OM.readValue(s, HashMap.class);
            res.put("result", result);
        } else if (chain.equals(NXT.name())) {
            String s = BlockchainHelper.getNxtAccountId(sendObject.getPassphrase());
            HashMap<String, Object> result = WalletKitHelper.OM.readValue(s, HashMap.class);
            res.put("result", result);
        } else {
            String s = BlockchainHelper.getARDRAccountId(sendObject.getPassphrase(), chain);
            HashMap<String, Object> result = WalletKitHelper.OM.readValue(s, HashMap.class);
            res.put("result", result);
        }

        return Response.ok(res).build();
    }

    /**
     * get all users who are beneficiaries
     *
     * @param apiKey
     * @param wallet
     * @param requestUser
     * @return
     */
    @Path("beneficiaries")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getBeneficiaries(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet, User requestUser) {
        LOGGER.info("Called getBeneficiaries");
        requestUser.setWalletCode(wallet);
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        SendObject sendObject = new SendObject();

        sendObject.setPassword(requestUser.getPassword());

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], ["
                    + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(requestUser.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            List<User> beneficiariesFromDB = UserDAO.getBeneficiaries(dbUser);
            List<User> beneficiaries = beneficiariesFromDB.stream().filter(x -> {
                try {
                    x.setKyc(OM.convertValue(UserDAO.getAccountKYC(x), HashMap.class));
                } catch (SQLException ex) {
                    Logger.getLogger(UserAPI.class.getName()).log(Level.SEVERE, null, ex);
                }
                return true;
            }).collect(Collectors.toList());

            res.put("result", beneficiaries);
        }

        return Response.ok(res).build();
    }

    @Path("become-beneficiary")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response becomeBeneficiary(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet,
            User requestUser) throws IOException {
        LOGGER.info("Called becomeBeneficiary");

        requestUser.setWalletCode(wallet);
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        SendObject sendObject = new SendObject();

        sendObject.setPassword(requestUser.getPassword());

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], ["
                    + e.getMessage() + "]");
        }
        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            dbUser.setBeneficiary(requestUser.isBeneficiary());
            try {
                WalletKitHelper.becomeBeneficiary(dbUser);
            } catch (SQLException e) {
                res.put("result", "Server error updating your account");
            }
            res.put("result", "successfull");
        }

        return Response.ok(res).build();
    }

    /**
     * Add new user.
     *
     * @param apiKey
     * @param requestUser
     * @return status of request (success | fail)
     */
    @Path("new")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createUser(@HeaderParam("apiKey") String apiKey, User requestUser) {
        LOGGER.info("Called createUser");
        LOGGER.log(Level.INFO, "API KEY: {0}", apiKey);
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        String passwordMD5 = BCrypt.hashpw(requestUser.getPassword(), BCrypt.gensalt(12));

        User dbUser = null;
        MessageDigest digest;
        String hash = "";
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(requestUser.getPassphrase().getBytes(StandardCharsets.UTF_8));
            hash = Base64.getEncoder().encodeToString(encodedhash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserAPI.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
            // dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error adding user with email [" + requestUser.getEmailAddress() + "], ["
                    + e.getMessage() + "]");
        }

        Group group = null;
        try {
            LOGGER.info(requestUser.getReferralCode());
            group = AdminDAO.findGroupByName(requestUser.getReferralCode());
            if (group == null) {
                res.put("response_text", "error");
                res.put("result", "Invalid Referral Code. Please enter a valid referral code");
            }
        } catch (Exception e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error adding user with email [" + requestUser.getEmailAddress() + "], ["
                    + e.getMessage() + "]");
        }

        SendObject sendObject = new SendObject();
        sendObject.setPassword(requestUser.getPassword());

        if (dbUser != null) {
            res.put("response_text", "error");
            res.put("result", "User with email address already exists");
        } else if (group != null) {
            requestUser.setHash(hash);
            requestUser.setPassword(passwordMD5);

            try {
                WalletKitHelper.addNewWallet(requestUser);
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        return Response.ok(res).build();
    }

    /**
     * Add new user.
     *
     * @param apiKey
     * @param requestUser
     * @return status of request (success | fail)
     */
    @Path("update")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateUserInfo(@HeaderParam("apiKey") String apiKey, User requestUser) throws SQLException {
        LOGGER.info("Called createUser");
        LOGGER.log(Level.INFO, "API KEY: {0}", apiKey);
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        SendObject sendObject = new SendObject();
        sendObject.setPassword(requestUser.getPassword());

        User dbUser;
        MessageDigest digest;
        String hash = "";
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(requestUser.getPassphrase().getBytes(StandardCharsets.UTF_8));
            hash = Base64.getEncoder().encodeToString(encodedhash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserAPI.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            dbUser = null;
        }
        if (dbUser == null) {
            String message = "Invalid login details: email";
            res.put("response_text", "error");
            res.put("result", message);
        } else {
            if (dbUser.getHash().equals(hash)) {
                res.put("result", dbUser);
            } else {
                String message = "The passphrase entered doesn't match your email address. Please enter the correct passphrase and email supplied during registration";
                res.put("response_text", "error");
                res.put("result", message);
                dbUser = null;
            }

            String refCode = requestUser.getRefCode() == null ? "" : requestUser.getRefCode();

            if (dbUser != null && refCode.equals("reset")) {
                String passwordMD5 = BCrypt.hashpw(requestUser.getPassword(), BCrypt.gensalt(12));
                dbUser.setPassword(passwordMD5);
                UserDAO.updatePassword(dbUser);
            }

            if (dbUser != null && !BCrypt.checkpw(requestUser.getPassword(), dbUser.getPassword())) {
                String message = "Invalid login details: password";
                res.put("response_text", "error");
                res.put("result", message);
                dbUser = null;
            }
        }

        if (dbUser != null) {
            try {
                WalletKitHelper.updateWallet(requestUser);
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
        return Response.ok(res).build();
    }

    /**
     * Get the blockchain transactions for the wallet
     *
     * @param apiKey
     * @param wallet
     * @param requestUser
     * @return
     */
    @Path("transactions")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTransactions(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet,
            User requestUser) {
        LOGGER.info("Called getTransactions");
        requestUser.setWalletCode(wallet);
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        SendObject sendObject = new SendObject();

        sendObject.setPassword(requestUser.getPassword());

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], ["
                    + e.getMessage() + "]");
        }

        if (requestUser.getEmailAddress().contains("guest-") && requestUser.getPassword().equals("guest")) {
            dbUser = new User();
            dbUser.setEmailAddress("guest");
            dbUser.setId(0l);
        }

        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            HashMap<String, Object> localRequestData = new HashMap<>();

            dbUser.setWalletCode(wallet);
            dbUser.setNetworkAddress(requestUser.getNetworkAddress());
            dbUser.setCurrencyId(requestUser.getCurrencyId());
            dbUser.setEquityId(requestUser.getEquityId());
            localRequestData.put("user", dbUser);

            AppMessenger appMessenger = new DefaultAppMessenger(localRequestData);

            res.put("result", WalletKitHelper.getTransactions(appMessenger, -1, -1));
        }

        return Response.ok(res).build();
    }

    /**
     * Get the blockchain transactions for the wallet
     *
     * @param apiKey
     * @param wallet
     * @param requestUser
     * @param startDate
     * @param endDate
     * @return
     */
    @Path("transactions/{startDate}/{endDate}")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTransactionsByDate(@HeaderParam("apiKey") String apiKey, @HeaderParam("wallet") String wallet,
            User requestUser, @PathParam("startDate") long startDate, @PathParam("endDate") long endDate) {
        LOGGER.info("Called getTransactions");
        requestUser.setWalletCode(wallet);
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        SendObject sendObject = new SendObject();

        sendObject.setPassword(requestUser.getPassword());

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + sendObject.getEmailAddress() + "], ["
                    + e.getMessage() + "]");
        }

        if (requestUser.getEmailAddress().contains("guest-") && requestUser.getPassword().equals("guest")) {
            dbUser = new User();
            dbUser.setEmailAddress("guest");
            dbUser.setId(0l);
        }

        if (dbUser == null || !BCrypt.checkpw(sendObject.getPassword(), dbUser.getPassword())) {
            res.put("response_text", "error");
            res.put("result", "User can not be found");
        } else {
            HashMap<String, Object> localRequestData = new HashMap<>();

            dbUser.setWalletCode(wallet);
            dbUser.setNetworkAddress(requestUser.getNetworkAddress());
            dbUser.setCurrencyId(requestUser.getCurrencyId());
            dbUser.setEquityId(requestUser.getEquityId());
            localRequestData.put("user", dbUser);

            AppMessenger appMessenger = new DefaultAppMessenger(localRequestData);

            res.put("result", WalletKitHelper.getTransactions(appMessenger, startDate, endDate));
        }

        return Response.ok(res).build();
    }

    @Path("add/kyc")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addKycInfo(@HeaderParam("apiKey") String apiKey, User requestUser) throws SQLException {
        LOGGER.info("Called add info to kyc");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            dbUser = null;
        }
        if (dbUser == null) {
            String message = "Invalid login details";
            res.put("response_text", "error");
            res.put("result", message);
        } else {
            if (BCrypt.checkpw(requestUser.getPassword(), dbUser.getPassword())) {
                requestUser.setId(dbUser.getId());
                // WalletKitHelper.updateBlockChain(requestUser);
            }
        }
        res.put("response_text", "success");
        res.put("result", "KYC Added to blockchain");
        return Response.ok(res).build();
    }

    @Path("send-confirmation-email")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response sendConfirmationEmail(@HeaderParam("apiKey") String apiKey, User requestUser) throws IOException, EmailException {
        LOGGER.info("send confirmation email called");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            dbUser = null;
        }
        if (dbUser == null) {
            String message = "Invalid login details: email";
            res.put("response_text", "error");
            res.put("result", message);
        } else {
            WalletKitHelper.sendConfirmationEmail(dbUser);
            res.put("response_code", 0);
            res.put("response_text", "success");
            res.put("result", "Confirmation email sent.");
        }

        return Response.ok(res).build();
    }

    /**
     * login user wallet
     *
     * @param apiKey
     * @param requestUser
     * @param maker
     * @return logged in user or null TODO: Check if there's data in ACCOUNT_KYC
     * table for this user, if yes, send the data to the BLOCKCHAIN and mark it
     * as sent.
     */
    @Path("login")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response login(@HeaderParam("apiKey") String apiKey, @HeaderParam("maker") boolean maker, User requestUser) throws SQLException {
        LOGGER.info("Called login");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if (APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        SendObject sendObject = new SendObject();
        sendObject.setPassword(requestUser.getPassword());

        User dbUser;
        MessageDigest digest;
        String hash = "";
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(requestUser.getPassphrase().getBytes(StandardCharsets.UTF_8));
            hash = Base64.getEncoder().encodeToString(encodedhash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserAPI.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            dbUser = null;
        }
        if (dbUser == null) {
            String message = "Invalid login details: email";
            res.put("response_text", "error");
            res.put("result", message);
        } else {
            if (dbUser.getHash().equals(hash)) {
                res.put("result", dbUser);
            } else {
                String message = "The passphrase entered doesn't match your email address. Please enter the correct passphrase and email supplied during registration";
                res.put("response_text", "error");
                res.put("result", message);
                dbUser = null;
            }

            String refCode = requestUser.getRefCode() == null ? "" : requestUser.getRefCode();

            if (dbUser != null && refCode.equals("reset")) {
                String passwordMD5 = BCrypt.hashpw(requestUser.getPassword(), BCrypt.gensalt(12));
                dbUser.setPassword(passwordMD5);
                UserDAO.updatePassword(dbUser);
            }

            if (dbUser != null && !BCrypt.checkpw(requestUser.getPassword(), dbUser.getPassword())) {
                String message = "Invalid login details: password";
                res.put("response_text", "error");
                res.put("result", message);
                dbUser = null;
            }
        }

        if (dbUser != null) {
            if (!dbUser.isActivated()) {
                res.put("response_text", "error");
                res.put("response_code", ResponseCodes.ACCOUNT_NOT_ACTIVATED_RESPONSE_CODE);
                res.put("result",
                        "Account is not yet activated. Please check your email for instructions on how to activate your account");
            } else {
                HashMap<String, Object> localRequestData = new HashMap<>();
                dbUser.setWalletType(requestUser.getWalletType());
                dbUser.setNetworkAddress(requestUser.getNetworkAddress());
                localRequestData.put("user", dbUser);
                if (maker) {
                    if (!UserDAO.isUserMaker(dbUser.getId())) {
                        res.put("response_text", "error");
                        res.put("response_code", -1);
                        res.put("result", "You don't have permission to login on this app");
                    } else {
                        AppMessenger appMessenger = new DefaultAppMessenger(localRequestData);
                        res.put("result", WalletKitHelper.login(appMessenger));
                    }
                } else {
                    AppMessenger appMessenger = new DefaultAppMessenger(localRequestData);
                    res.put("result", WalletKitHelper.login(appMessenger));
                }
            }
        }
        return Response.ok(res).build();
    }
}
