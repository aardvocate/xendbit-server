/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.api.utils;

import com.xendbit.xendserver.models.BuyRequest;
import com.xendbit.xendserver.models.Exchange;
import com.xendbit.xendserver.models.SendObject;
import com.xendbit.xendserver.models.User;
import com.xendbit.xendserver.models.dao.BuyRequestDAO;
import com.xendbit.xendserver.models.dao.ExchangeDAO;
import com.xendbit.xendserver.models.dao.UserDAO;

import org.apache.commons.mail.EmailException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.codehaus.jackson.map.ObjectMapper;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.xendbit.xendserver.models.utils.DAOUtils;
import edu.self.kraken.api.KrakenApi;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

/**
 * @author aardvocate
 */
@SuppressWarnings("unchecked")
public class WalletKitHelper {

    //public static final String FLUTTERWAVE_API_KEY = "ts_0ISWVD5P291KFQ2OD10L";
    public static final String FLUTTERWAVE_API_KEY = "lv_CMAD8QBV91B4ASIKYXEI";
    //public static final String FLUTTERWAVE_API_SECRET = "ts_03VU7OWL5M4WUMIGQDVJCNTH4LW7CN";
    public static final String FLUTTERWAVE_API_SECRET = "lv_T96MVI4YDMRFYSJX9LI9DJ3D2CORZS";
    public static final String FLUTTERWAVE_WALLET_LOCK = "@bsolute";

    public static final long ONE_WEI = 1000000000000000000L;
    public static final long ONE_NXT = 100000000L;

    public static Properties p = DAOUtils.p;

    public static final ObjectMapper OM = new ObjectMapper();

    protected static final Logger LOGGER = Logger.getLogger(WalletKitHelper.class.getName());

    public static KrakenApi krakenApi = new KrakenApi();

    static {
        krakenApi.setKey("ZZt4BDyY679oRitCk2iQjsfduZV1sU9/LlRim5mgkeVfDn6kfvSiLydA");
        krakenApi.setSecret("4/e7DjOVyrEao0+/LqjekP5odDqJ+4Q0i8XF8DV23ltzpLW9R7B+CmGfn8HoHSOlgHheuSmVU7j6MoFM0K7QPQ==");
        //syncKrakenBalanceWithLocalTrades();
    }

    public static CloseableHttpClient registerHttps() {
        int timeout = 60000;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        CloseableHttpClient client = null;
        try {
            SSLContext sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, (certificate, authType) -> true).build();

            client = HttpClients.custom()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setDefaultRequestConfig(config)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    public static void sendNewBuyRequestEmail(String refCode) {
        String url = WalletKitHelper.p.getProperty("send.new.buy.request.email.url") + "/" + refCode;
        LOGGER.log(Level.INFO, "URL: {0}", url);

        CloseableHttpClient client = registerHttps();
        HttpPost postRequest = new HttpPost(url);
        postRequest.setHeader("content-type", "application/json");
        postRequest.setHeader("Accept", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public static long get2FACode(User requestUser) {
        long code = Math.abs(new Random(System.nanoTime()).nextLong() % 10000);
//send_new_buy_request_email_url.url
        String url = WalletKitHelper.p.getProperty("send2fa.url");
        LOGGER.log(Level.INFO, "URL: {0}", url);

        CloseableHttpClient client = registerHttps();
        HttpPost postRequest = new HttpPost(url);
        postRequest.setHeader("content-type", "application/json");
        postRequest.setHeader("Accept", "application/json");

        HashMap<String, Object> fields = new HashMap<>();
        fields.put("code", code);
        fields.put("emailAddress", requestUser.getEmailAddress());

        try {
            String jsonString = WalletKitHelper.OM.writeValueAsString(fields);
            StringEntity entity = new StringEntity(jsonString);
            entity.setContentType("application/json");

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return code;
    }

    //Posted less than 30 mins agi
    private static boolean isNew(BuyRequest x) {
        long thirtyMins = 30 * 60 * 1000;
        long now = new Date().getTime();
        return now - x.getRequestTime().getTime() < thirtyMins;
    }

    public static double howMuchCanWeSell(String ticker) throws Exception {
        double balance = BlockchainHelper.howMuchCanWeSell(ticker);
//        List<BuyRequest> buyRequests = BuyRequestDAO.findBuyRequestsByStatus("NEW");
//        List<BuyRequest> newRequests = buyRequests.stream().filter(x -> isNew(x)).collect(Collectors.toList());
//
//        //find buyRequests older than 30 mins and set them to PENDING automagically.                   
//        buyRequests.forEach(x -> {
//            if (!isNew(x)) {
//                BuyRequestDAO.updateBuyRequestStringColumn(x.getId(), "STATUS", "PENDING");
//            }
//        });
//
//        double btcValues = newRequests.stream().map(x -> x.getBitcoinAmount()).collect(Collectors.summarizingDouble(Number::doubleValue)).getSum();
//        balance = balance - btcValues;

        //List<Exchange> exchanges = ExchangeDAO.findAdminSellRequests();

        //double btcValues = exchanges.stream().map(x -> x.getAmountToSell()).collect(Collectors.summarizingDouble(Number::doubleValue)).getSum();
        //balance = balance - btcValues;
        return balance >= 0 ? balance : 0.0;
    }

//    private static void syncKrakenBalanceWithLocalTrades() {
//        new Thread() {
//            @Override
//            public void run() {
//                try {
//                    double balance = WalletKitHelper.howMuchCanWeSell("BTC");
//                    if (balance == 0) {
//                        List<Exchange> exchanges = ExchangeDAO.findAdminSellRequests();
//                        exchanges.forEach(x -> {
//                            ExchangeDAO.updateTradeStatus(x.getTrxId(), "CANCELLED_BY_API");
//                        });
//                    }
//                    Thread.sleep(5 * 1000 * 60);
//                } catch (Exception ex) {
//                    LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
//                }
//            }
//
//        }.start();
//    }

    public WalletKitHelper() {
    }

//    public static void updateBlockChain(User u) throws SQLException {
//        final User.AccountKYC kyc = UserDAO.getAccountKYC(u);
//        if (kyc == null) {
//            return;
//        }
//        sendCoinToUser(u);
//
//        new Thread() {
//            boolean shouldRun = true;
//
//            @Override
//            public void run() {
//                while (shouldRun) {
//                    String name = kyc.getSurName() + " " + kyc.getFirstName() + " " + kyc.getMiddleName();
//                    String jsonInfo = null;
//                    try {
//                        jsonInfo = OM.writeValueAsString(kyc);
//                        LOGGER.info(jsonInfo);
//                        LOGGER.info(u.getPassphrase());
//                        String s = BlockchainHelper.setAccountInfo(u.getPassphrase(), name, jsonInfo);
//
//                        HashMap<String, Object> result = WalletKitHelper.OM.readValue(s, HashMap.class);
//
//                        if (result.containsKey("errorDescription")) {
//                            if (result.get("errorDescription").equals("Unknown account")) {
//                                //wait 30 secs
//                                Thread.sleep(30000);
//                                shouldRun = true;
//                            } else {
//                                shouldRun = false;
//                            }
//                        } else {
//                            shouldRun = false;
//                        }
//
//                        if (!shouldRun) {
//                            UserDAO.setKycSynced(u);
//                        }
//                    } catch (IOException e) {
//                        shouldRun = false;
//                        e.printStackTrace();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }.start();
//    }
    public static void sendCoinToUser(User u) {
        sendCoinToUser(u, 100000);
    }

    public static void sendCoinToUser(User u, long val) {
        SendObject sendObject = new SendObject();
        sendObject.setToAddress(u.getNetworkAddress());
        sendObject.setPassphrase("Baba fi owo kan idodo omo oni dodo ni dodo ilu wa");
        long value = ONE_NXT * val;
        sendObject.setBtcValue(String.valueOf(value));
        BlockchainHelper.pushTx(sendObject, User.WALLET_CODES.XND.toString());
    }

    public static void addNewWallet(User requestUser) {
        try {
            if (requestUser.getAccountType().equals("ADVANCED") && !requestUser.getBankCode().equals("000")) {
                SendObject sendObject = new SendObject();
                sendObject.setBankCode(requestUser.getBankCode());
                sendObject.setAccountNumber(requestUser.getAccountNumber());
                String accountName = MoneyWaveAPI.verifyBankAccount(sendObject);
                requestUser.setAccountName(accountName);
            }

            if (requestUser.getBankCode().equals("000")) {
                requestUser.setAccountName("Evaluation Account");
            }

            UserDAO.addNewUser(requestUser);
            requestUser.setId(UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress()).getId());

            String s = BlockchainHelper.getXndAccountId(requestUser.getPassphrase());
            HashMap<String, Object> result = WalletKitHelper.OM.readValue(s, HashMap.class);
            requestUser.setPublicKey(result.get("publicKey").toString());
            requestUser.setNetworkAddress(result.get("accountRS").toString());

            String kycDataPath = saveImage(requestUser);
            UserDAO.addAccountKYC(requestUser, kycDataPath);

            sendConfirmationEmail(requestUser);
        } catch (XendException | IOException | SQLException | EmailException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public static void updateWallet(User requestUser) {
        try {
            if (requestUser.getAccountType().equals("ADVANCED") && !requestUser.getBankCode().equals("000")) {
                SendObject sendObject = new SendObject();
                sendObject.setBankCode(requestUser.getBankCode());
                sendObject.setAccountNumber(requestUser.getAccountNumber());
                String accountName = MoneyWaveAPI.verifyBankAccount(sendObject);
                requestUser.setAccountName(accountName);
            }

            if (requestUser.getBankCode().equals("000")) {
                requestUser.setAccountName("Evaluation Account");
            }

            requestUser.setId(UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress()).getId());
            UserDAO.updateUserInfo(requestUser);
            UserDAO.updateUserKYCInfo(requestUser);
        } catch (XendException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        } catch (SQLException ex) {
            Logger.getLogger(WalletKitHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String saveImage(User requestUser) {
        String url = WalletKitHelper.p.getProperty("saveimage.url");
        LOGGER.log(Level.INFO, "URL: {0}", url);

        CloseableHttpClient client = registerHttps();
        HttpPost postRequest = new HttpPost(url);
        postRequest.setHeader("content-type", "application/json");
        postRequest.setHeader("Accept", "application/json");

        HashMap<String, Object> fields = new HashMap<>();
        fields.put("idImage", requestUser.getIdImage());
        fields.put("emailAddress", requestUser.getEmailAddress());
        LOGGER.info("ID IMAGE");
        LOGGER.info(requestUser.getIdImage());

        try {
            String jsonString = WalletKitHelper.OM.writeValueAsString(fields);
            StringEntity entity = new StringEntity(jsonString);
            entity.setContentType("application/json");

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
            if (response.containsKey("response")) {
                return response.get("response").toString();
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    public static void sendConfirmationEmail(User dbUser) throws IOException, EmailException {
        String url = WalletKitHelper.p.getProperty("sendemail.url");
        LOGGER.log(Level.INFO, "URL: {0}", url);

        CloseableHttpClient client = registerHttps();
        HttpPost postRequest = new HttpPost(url);
        postRequest.setHeader("content-type", "application/json");
        postRequest.setHeader("Accept", "application/json");

        HashMap<String, Object> fields = new HashMap<>();
        fields.put("firstName", dbUser.getFirstName());
        fields.put("surName", dbUser.getSurName());
        fields.put("middleName", dbUser.getMiddleName());
        fields.put("emailAddress", dbUser.getEmailAddress());

        try {
            String jsonString = WalletKitHelper.OM.writeValueAsString(fields);
            StringEntity entity = new StringEntity(jsonString);
            entity.setContentType("application/json");

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.log(Level.INFO, "Response String = {0}", s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public static boolean buyDirect(AppMessenger appMessenger) {
        Map<String, Object> objects = appMessenger.getObjects();
        User user = (User) objects.get("user");
        SendObject sendObject = (SendObject) objects.get("send-object");
        String[] bankData = sendObject.getSellerToAddress().split(":");

        BuyRequest buyRequest = new BuyRequest();
        buyRequest.setAmount(sendObject.getAmountToSell());
        buyRequest.setBitcoinAddress(sendObject.getBuyerToAddress());
        buyRequest.setBitcoinAmount(sendObject.getAmountToRecieve());
        buyRequest.setReference(sendObject.getSellOrderTransactionId());
        buyRequest.setAccountNumber(bankData[0]);
        buyRequest.setBank(bankData[1]);
        buyRequest.setStatus("NEW");
        buyRequest.setUser(user);

        BuyRequestDAO.addBuyRequest(buyRequest);

        sendNewBuyRequestEmail(buyRequest.getReference());
        return true;
    }

    public static boolean addNewTrade(AppMessenger appMessenger) {
        Map<String, Object> objects = appMessenger.getObjects();
        User user = (User) objects.get("user");
        SendObject sendObject = (SendObject) objects.get("send-object");

        Exchange exchange = new Exchange();
        if (sendObject.isDirectSend()) {
            exchange.setStatus(ExchangeDAO.STATUS.SUCCESS.toString());
        }

        exchange.setStatus(ExchangeDAO.STATUS.ORDER_PLACED.toString());
        exchange.setActive(true);
        exchange.setRate(sendObject.getRate());
        exchange.setFromCoin(sendObject.getFromCoin());
        exchange.setToCoin(sendObject.getToCoin());
        exchange.setDatetime(new Date());
        exchange.setAmountToSell(sendObject.getAmountToSell());
        exchange.setAmountToRecieve(sendObject.getAmountToRecieve());
        exchange.setSellerFromAddress(sendObject.getSellerFromAddress());
        exchange.setSellerToAddress(sendObject.getSellerToAddress());
        exchange.setSellerId(user.getId());
        exchange.setTrxId(UUID.randomUUID().toString());
        exchange.setBrokerAccount(sendObject.getBrokerAccount());
        exchange.setFees(sendObject.getFees());
        exchange.setSellerIsAdmin(sendObject.isSellerIsAdmin());

        ExchangeDAO.addTrade(exchange);

        return true;
    }

    public static HashMap<String, Object> login(AppMessenger appMessenger) throws SQLException {
        appMessenger.done();
        HashMap<String, Object> responseMap = new HashMap<>();
        Map<String, Object> objects = appMessenger.getObjects();
        User user = (User) objects.get("user");
        user.setKyc(OM.convertValue(UserDAO.getAccountKYC(user), HashMap.class));
        user.setPassword("********************");
        responseMap.put("user", user);
        startMonitorThread(user);
        if (user.getWalletType() != null && user.getWalletType().equals("nse-investements")) {
            checkBalanceAndTopup(user);
        }
        return responseMap;
    }

    private static void checkBalanceAndTopup(User user) {
        user.setWalletCode(User.WALLET_CODES.XND.toString());
        long time = new Date().getTime();
        HashMap<String, Object> transactions = TransactionsGetter.getTransactions(user, time, time);
        long balance = Math.round(Double.parseDouble(transactions.get("balance").toString()));

        if (balance <= 2) {
            sendCoinToUser(user, (100 - balance));
        }
    }

    private static HashMap<String, Thread> runningThreads = new HashMap<>();

    private static void startMonitorThread(final User user) {
        Thread t = runningThreads.get(user.getEmailAddress());
        if (t == null) {
            t = new Thread(() -> {
                while (true) {
                    checkExpiredTrades(user);
                    try {
                        Thread.sleep(60 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

            t.start();
            runningThreads.put(user.getEmailAddress(), t);
        }
    }

    private static void checkExpiredTrades(User user) {
        List<Exchange> pendingTrades = ExchangeDAO.getPendingTrades(user);
        assert pendingTrades != null;
        pendingTrades.forEach(x -> {
            Date trxDate = x.getDatetime();
            LOGGER.log(Level.INFO, "CET: Transaction Date [{0}]", trxDate);
            LOGGER.log(Level.INFO, "CET: NOW [{0}]", new Date());
            LOGGER.log(Level.INFO, "CET:  Diff [{0}]", (new Date().getTime() - trxDate.getTime()));

            if (new Date().getTime() - trxDate.getTime() >= 10 * 60 * 1000) {
                //this trade has expired.
                ExchangeDAO.updateTradeStatus(x.getTrxId(), ExchangeDAO.STATUS.ORDER_PLACED.toString());
            }
        });
    }

    public static HashMap<String, Object> getTransactions(AppMessenger appMessenger, long startDate, long endDate) {
        Map<String, Object> objects = appMessenger.getObjects();
        User user = (User) objects.get("user");
        return TransactionsGetter.getTransactions(user, startDate, endDate);
    }

    public static void becomeBeneficiary(User dbUser) throws SQLException {
        UserDAO.becomeBeneficiary(dbUser);
    }
}

//{"text": "Ethereum", "value": "ETH", "symbol": "ETH", "ticker_symbol":"ethereum", "xend.fees": 0.000625, "block.fees": 0.0008, "xend.address": "0x5b61c8d90ca057d707d3a615dcacb7f03d372bce", "multiplier": 1000000000000000000, "url": "https://rinkeby.infura.io/qxz5FjxeA5Mg2zt2ieOg", "contract": "0x2dd6856f2a90d05071a392acd160b98652d72bbb"},
