/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.api.utils;

import java.util.Map;

/**
 *
 * @author aardvocate
 */
public interface AppMessenger {
    void done();
    void error();
    Map<String, Object> getObjects();
}
