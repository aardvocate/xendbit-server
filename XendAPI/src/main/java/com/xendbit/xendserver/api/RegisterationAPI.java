package com.xendbit.xendserver.api;


import com.xendbit.xendserver.models.User;
import com.xendbit.xendserver.models.dao.APIUserDAO;
import com.xendbit.xendserver.models.dao.SchoolDAO;
import com.xendbit.xendserver.models.dao.UserDAO;

import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;




/**
 * @author aardvocate
 */

@Path("register")
public class RegisterationAPI {

    protected static final Logger LOGGER = Logger.getLogger(RegisterationAPI.class.getName());

/**
 * check if the user registration is completed.
 * @param apiKey
 * @param requestUser
 * @return 
 */
    @Path("status")
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response checkRegistrationStatus(@HeaderParam("apiKey") String apiKey, User requestUser){
        LOGGER.info("Called checkRegistrationStatus");
        Map<String, Object> res = new HashMap<>();
        res.put("response_code", 0);
        res.put("response_text", "success");

        if(APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }
        User dbUser = null;
        try {
            dbUser = UserDAO.findByColumn("EMAIL", requestUser.getEmailAddress());
        } catch (SQLException e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "Server Error getting user with email [" + requestUser.getEmailAddress() + "], [" + e.getMessage() + "]");
        }
        if (dbUser == null) {
            String message = "Please Wait...";
            res.put("response_text", "error");
            res.put("result", message);
        } else {
            res.put("result", dbUser);
        }
        return Response.ok(res).build();
    }

    @Path("schools")
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSchools(@HeaderParam("apiKey") String apiKey) {
        LOGGER.info("getSchools");
        Map<String, Object> res = new HashMap<>();

        if(APIUserDAO.getAPIUser(apiKey) == null) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            res.put("result", "API Authentication failed.");
            return Response.ok(res).build();
        }

        try {
            res.put("response_code", 0);
            res.put("response_text", "success");
            res.put("result", SchoolDAO.getSchools());
        } catch (Exception e) {
            res.put("response_code", 0);
            res.put("response_text", "error");
            e.printStackTrace();
        }

        return Response.ok(res).build();
    }

}
