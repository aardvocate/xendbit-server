pragma solidity ^0.4.0;

contract SendTransaction {
    address xend;

    constructor() public {
        xend = msg.sender;
    }

    function sendCoin(address to, uint256 xendFees) public payable returns(bool success) {
        uint256 value = msg.value;
        to.transfer(value);
        xend.transfer(xendFees);
        return true;
    }

    function kill() public { //self-destruct function,
        if (msg.sender == xend) {
            selfdestruct(xend);
        }
    }
}

//0x2dd6856F2a90d05071A392aCd160B98652d72BbB
//0x5b61c8d90ca057d707d3a615dcacb7f03d372bce
//0x3aeed5a9b023dc1265274055c1f98f33e71a9caac3b27b16e1a8448a82885df0