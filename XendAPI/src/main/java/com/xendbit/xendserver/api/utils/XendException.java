package com.xendbit.xendserver.api.utils;

import java.util.UUID;

public class XendException extends Exception {

    public static final long serialVersionUID = UUID.randomUUID().getMostSignificantBits();
    public XendException(String exception) {
        super(exception);
    }

    public XendException(Exception e) {
        super(e);
    }    
}
