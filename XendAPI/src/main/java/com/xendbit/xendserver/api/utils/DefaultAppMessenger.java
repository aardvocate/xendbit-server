/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.api.utils;

import java.util.Map;

/**
 *
 * @author aardvocate
 */

public class DefaultAppMessenger implements AppMessenger {

    private Map<String, Object> objects;

    public DefaultAppMessenger(Map<String, Object> objects) {
        this.objects = objects;
    }

    @Override
    public void done() {
    }

    @Override
    public void error() {
    }
    
    public Map<String, Object> getObjects() {
		return objects;
	}

    public void setObjects(Map<String, Object> objects) {
        this.objects = objects;
    }
}
