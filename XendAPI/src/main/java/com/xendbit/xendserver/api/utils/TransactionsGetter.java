package com.xendbit.xendserver.api.utils;

import com.xendbit.xendserver.models.dao.ExchangeDAO;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import com.xendbit.xendserver.models.Exchange;
import com.xendbit.xendserver.models.User;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import com.xendbit.xendserver.models.Transaction;

@SuppressWarnings("unchecked")
public class TransactionsGetter {

    private static final Logger LOGGER = Logger.getLogger(TransactionsGetter.class.getName());

    public static HashMap<String, Object> getTransactions(User user, long startDate, long endDate) {
        if (user.getCurrencyId() != null && user.getCurrencyId().length() > 0) {
            return getXndTokenUserDisplayInfo(user);
        }

        if (user.getEquityId() != null && user.getEquityId().length() > 0) {
            return getEquitiesUserDisplayInfo(user, startDate, endDate);
        }

        User.WALLET_CODES wc = User.WALLET_CODES.valueOf(user.getWalletCode());

        switch (wc) {
            case BTC:
            case LTC:
            case DASH:
            case BTCTEST:
            case LTCTEST:
            case DASHTEST:
                return getUserDisplayInfo(user, startDate, endDate);
            case ETH:
            case ETHTEST:
                return getEthereumUserDisplayInfo(user, wc);
            case XND:
                return getXndUserDisplayInfo(user);
            case NXT:
                return getNxtUserDisplayInfo(user);
            case ARDOR:
                return getARDRUserDisplayInfo(user, 1);
            case IGNIS:
                return getARDRUserDisplayInfo(user, 2);
        }

        return null;
    }

    public static List<Transaction> getUnspentOutputs(String address, String wallet) {
        List<Transaction> transactions = new ArrayList<>();

        String unspentUrl = WalletKitHelper.p.getProperty("unspent.url") + "/" + address + "/" + wallet;
        LOGGER.info(unspentUrl);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(unspentUrl);
        getRequest.setHeader("content-type", "application/json");
        getRequest.setHeader("Accept", "application/json");
        getRequest.setHeader("apiKey", "oalkuisnetgauyno");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);

            List<HashMap<String, Object>> txs = (List<HashMap<String, Object>>) response.get("result");

            txs.stream().forEach(tx -> {
                Transaction transaction = new Transaction();
                transaction.setHash(tx.get("txid").toString());
                transaction.setIndex(Integer.parseInt(tx.get("vout").toString()));
                transaction.setValue(Double.parseDouble(tx.get("amount").toString()));
                transaction.setConfirmations(Long.parseLong(tx.get("confirmations").toString()));
                transactions.add(transaction);
            });

            return transactions;
        } catch (IOException | UnsupportedOperationException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    public static HashMap<String, Object> getUserDisplayInfo(User user, long startDate, long endDate) {
        HashMap<String, Object> userInfo = new HashMap<>();

        String txUrl = WalletKitHelper.p.getProperty("blockchain.tx.url") + user.getWalletCode();
        String baseUrl = WalletKitHelper.p.getProperty("blockchain.api.url");
        String transactionsUrl = baseUrl + "address/" + user.getWalletCode() + "/" + user.getNetworkAddress();

        LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transactionsUrl);
        getRequest.setHeader("content-type", "application/json");
        getRequest.setHeader("Accept", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            HashMap<String, Object> data = (HashMap<String, Object>) response.get("data");

            double balance = Double.parseDouble(data.get("balance").toString());
            double pendingValue = Double.parseDouble(data.get("pending_value").toString());
            // only add pending if it's negative
            if (pendingValue < 0) {
                balance = balance + pendingValue;
            }
            LOGGER.log(Level.INFO, "Balance + Pending: {0}", balance);

            double received = Double.parseDouble(data.get("received_value").toString());
            double spent = Math.abs(balance - received);

            List<Exchange> exchanges = ExchangeDAO.getEscrowUserTrades(user);
            assert exchanges != null;
            double escrow = getEscrow(exchanges, user);

            userInfo.put("balance", balance - escrow);
            userInfo.put("received", received);
            userInfo.put("spent", spent);
            userInfo.put("escrow", escrow);

            List<HashMap<String, Object>> txs = (List<HashMap<String, Object>>) data.get("txs");

            List<Transaction> transactions = new ArrayList<>();

            txs.stream().forEach((HashMap<String, Object> tx) -> {
                Transaction transaction = new Transaction();
                transaction.setUrl(txUrl + "/" + tx.get("txid").toString());

                if (tx.containsKey("outgoing")) {
                    double value = Double
                            .parseDouble(((HashMap<String, Object>) tx.get("outgoing")).get("value").toString());
                    transaction.setValue(value);
                    transaction.setIncoming(false);
                } else {
                    transaction.setValue(
                            Double.parseDouble(((HashMap<String, Object>) tx.get("incoming")).get("value").toString()));
                    transaction.setIncoming(true);
                }

                long time = Long.parseLong(tx.get("time").toString()) * 1000;
                transaction.setTime(time);
                transaction.setIndex(0);
                transaction.setHash(tx.get("txid").toString());
                transaction.setConfirmations(Long.parseLong(tx.get("confirmations").toString()));
                LOGGER.log(Level.INFO, "StartDate: {0}, EndDate: {1}, Time: {2}",
                        new Object[] { startDate, endDate, time });
                if (startDate > 0) {
                    if (time >= startDate && time <= endDate) {
                        transactions.add(transaction);
                    }
                } else {
                    transactions.add(transaction);
                }
            });

            userInfo.put("transactions", transactions);
            return userInfo;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    private static void getXndTokenBalance(HashMap<String, Object> userInfo, User user)
            throws UnsupportedEncodingException {
        HashMap<String, Object> currencies = BlockchainHelper.getXndAccountCurrencies(user.getNetworkAddress(), user);

        List<Exchange> exchanges = ExchangeDAO.getEscrowUserTrades(user);
        assert exchanges != null;
        double escrow = getEscrow(exchanges, user);

        double balance = 0;
        if (currencies.containsKey("units")) {
            balance = (Double.parseDouble(currencies.get("units").toString()));
            Long decimals = Long.parseLong(currencies.get("decimals").toString());
            balance = balance / (Math.pow(10, decimals));
        }

        userInfo.put("balance", balance - escrow);
        userInfo.put("escrow", escrow);
        userInfo.put("received", 0);
        userInfo.put("spent", 0);
    }

    private static void getXendBalance(HashMap<String, Object> userInfo, User user)
            throws UnsupportedEncodingException {
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");

        String balanceUrl = BASE_URL + "requestType=getBalance" + "&account="
                + URLEncoder.encode(user.getNetworkAddress(), "UTF-8");
        LOGGER.log(Level.INFO, "URL: {0}", balanceUrl);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(balanceUrl);
        getRequest.setHeader("content-type", "application/json");

        userInfo.put("balance", 0);
        userInfo.put("received", 0);
        userInfo.put("spent", 0);
        userInfo.put("escrow", 0);

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            long balance = (Long.parseLong(response.get("balanceNQT").toString())) / WalletKitHelper.ONE_NXT;

            List<Exchange> exchanges = ExchangeDAO.getEscrowUserTrades(user);
            assert exchanges != null;
            double escrow = getEscrow(exchanges, user);

            userInfo.put("balance", balance - escrow);
            userInfo.put("escrow", escrow);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private static void getNxtBalance(HashMap<String, Object> userInfo, User user) throws UnsupportedEncodingException {
        String BASE_URL = WalletKitHelper.p.getProperty("nxt.api.url");

        String balanceUrl = BASE_URL + "requestType=getBalance" + "&account="
                + URLEncoder.encode(user.getNetworkAddress(), "UTF-8");
        LOGGER.log(Level.INFO, "URL: {0}", balanceUrl);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(balanceUrl);
        getRequest.setHeader("content-type", "application/json");

        userInfo.put("balance", 0);
        userInfo.put("received", 0);
        userInfo.put("spent", 0);
        userInfo.put("escrow", 0);

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            long balance = (Long.parseLong(response.get("balanceNQT").toString())) / WalletKitHelper.ONE_NXT;

            List<Exchange> exchanges = ExchangeDAO.getEscrowUserTrades(user);
            assert exchanges != null;
            double escrow = getEscrow(exchanges, user);

            userInfo.put("balance", balance - escrow);
            userInfo.put("escrow", escrow);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     *
     * @param userInfo
     * @param user
     * @param chain    ARDR = 1, IGNIS = 2
     * @throws UnsupportedEncodingException
     */
    private static void getARDRBalance(HashMap<String, Object> userInfo, User user, int chain)
            throws UnsupportedEncodingException {
        String BASE_URL = WalletKitHelper.p.getProperty("ardr.api.url");

        String balanceUrl = BASE_URL + "requestType=getBalance&chain=" + chain + "&account="
                + URLEncoder.encode(user.getNetworkAddress(), "UTF-8");

        LOGGER.log(Level.INFO, "URL: {0}", balanceUrl);
        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(balanceUrl);
        getRequest.setHeader("content-type", "application/json");

        userInfo.put("balance", 0);
        userInfo.put("received", 0);
        userInfo.put("spent", 0);
        userInfo.put("escrow", 0);

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            long balance = (Long.parseLong(response.get("balanceNQT").toString())) / WalletKitHelper.ONE_NXT;

            List<Exchange> exchanges = ExchangeDAO.getEscrowUserTrades(user);
            assert exchanges != null;
            double escrow = getEscrow(exchanges, user);

            userInfo.put("balance", balance - escrow);
            userInfo.put("escrow", escrow);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private static HashMap<String, Object> getXndTokenUserDisplayInfo(User user) {
        HashMap<String, Object> userInfo = new HashMap<>();
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");

        String transfersUrl = BASE_URL + "requestType=getCurrencyTransfers&currency=" + user.getCurrencyId()
                + "&account=" + user.getNetworkAddress().replaceAll("XND", "NXT") + "&includeCurrencyInfo=true";
        LOGGER.info(transfersUrl);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transfersUrl);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            List<HashMap<String, Object>> result = (ArrayList<HashMap<String, Object>>) response.get("transfers");

            List<Transaction> transactions = new ArrayList<>();

            final String TX_URL = "http://localhost:8987/nxt?=%2Fnxt&requestType=getTransaction/";
            if (result != null) {
                result.stream().forEach(res -> {
                    Transaction transaction = new Transaction();

                    transaction.setUrl(TX_URL + res.get("transfer").toString());

                    transaction.setConfirmations(6);
                    transaction.setIndex(Integer.parseInt(res.get("height").toString()));
                    transaction.setHash(res.get("transfer").toString());
                    Double value = Double.parseDouble(res.get("units").toString());
                    Long decimals = Long.parseLong(res.get("decimals").toString());
                    value = value / (Math.pow(10, decimals));
                    transaction.setValue(value);

                    if (res.get("senderRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(true);
                        transaction.setIncoming(false);
                    } else if (res.get("recipientRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(false);
                        transaction.setIncoming(true);
                    }

                    transactions.add(transaction);
                });
            }

            userInfo.put("transactions", transactions);
            getXndTokenBalance(userInfo, user);
            return userInfo;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    private static HashMap<String, Object> getEquitiesUserDisplayInfo(User user, long startDate, long endDate) {
        long EPOCH_BEGINNING = Long.parseLong(WalletKitHelper.p.getProperty("epoch.beginning"));

        HashMap<String, Object> userInfo = new HashMap<>();
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");

        String transfersUrl = BASE_URL + "requestType=getAssetTransfers&asset=" + user.getEquityId() + "&account="
                + user.getNetworkAddress().replaceAll("XND", "NXT") + "&includeAssetInfo=true";
        LOGGER.info(transfersUrl);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transfersUrl);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            List<HashMap<String, Object>> result = (ArrayList<HashMap<String, Object>>) response.get("transfers");

            List<Transaction> transactions = new ArrayList<>();

            final String TX_URL = "http://localhost:8987/nxt?=%2Fnxt&requestType=getTransaction/";
            if (result != null) {
                result.stream().forEach(res -> {
                    Transaction transaction = new Transaction();

                    transaction.setUrl(TX_URL + res.get("assetTransfer").toString());

                    transaction.setConfirmations(6);
                    transaction.setIndex(Integer.parseInt(res.get("height").toString()));
                    transaction.setHash(res.get("assetTransfer").toString());
                    Double value = Double.parseDouble(res.get("quantityQNT").toString());
                    Long decimals = Long.parseLong(res.get("decimals").toString());
                    value = value / (Math.pow(10, decimals));
                    transaction.setValue(value);

                    if (res.get("senderRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(true);
                        transaction.setIncoming(false);
                    } else if (res.get("recipientRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(false);
                        transaction.setIncoming(true);
                    }
                    long time = Long.parseLong(res.get("timestamp").toString()) * 1000;
                    time += (EPOCH_BEGINNING - 500);
                    transaction.setTime(time);
                    transactions.add(transaction);
                });
            }

            userInfo.put("transactions", transactions);
            getEquitiesBalance(userInfo, user);
            return userInfo;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    private static void getEquitiesBalance(HashMap<String, Object> userInfo, User user)
            throws UnsupportedEncodingException {
        HashMap<String, Object> currencies = BlockchainHelper.getAccountEquities(user.getNetworkAddress(),
                user.getEquityId());

        List<Exchange> exchanges = ExchangeDAO.getEscrowUserTrades(user);
        assert exchanges != null;
        double escrow = getEscrow(exchanges, user);

        double balance = 0;
        if (currencies.containsKey("asset")) {
            balance = (Double.parseDouble(currencies.get("quantityQNT").toString()));
            Long decimals = Long.parseLong(currencies.get("decimals").toString());
            balance = balance / (Math.pow(10, decimals));
        }

        userInfo.put("balance", balance - escrow);
        userInfo.put("escrow", escrow);
        userInfo.put("received", 0);
        userInfo.put("spent", 0);
    }

    private static HashMap<String, Object> getXndUserDisplayInfo(final User user) {
        HashMap<String, Object> userInfo = new HashMap<>();
        String BASE_URL = WalletKitHelper.p.getProperty("xend.api.url");

        String transactionsUrl = null;
        try {
            transactionsUrl = BASE_URL + "requestType=getBlockchainTransactions" + "&account="
                    + URLEncoder.encode(user.getNetworkAddress(), "UTF-8");
            LOGGER.info(transactionsUrl);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        if (transactionsUrl == null) {
            return null;
        }

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transactionsUrl);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");

            LOGGER.info(s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            List<HashMap<String, Object>> result = (ArrayList<HashMap<String, Object>>) response.get("transactions");

            List<Transaction> transactions = new ArrayList<>();

            result.stream().forEach(res -> {
                if (res.get("type").toString().equals("0")) {
                    Transaction transaction = new Transaction();

                    long confirmations = Long.parseLong(res.get("confirmations").toString());
                    transaction.setConfirmations(confirmations);
                    transaction.setIndex(Integer.parseInt(res.get("transactionIndex").toString()));
                    transaction.setHash(res.get("transaction").toString());
                    Double value = Double.parseDouble(res.get("amountNQT").toString());
                    transaction.setValue(value != 0 ? value / WalletKitHelper.ONE_NXT : value);

                    if (res.get("senderRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(true);
                        transaction.setIncoming(false);
                    } else if (res.get("recipientRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(false);
                        transaction.setIncoming(true);
                    }

                    transactions.add(transaction);
                }
            });

            userInfo.put("transactions", transactions);
            getXendBalance(userInfo, user);
            LOGGER.info(userInfo.toString());
            return userInfo;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    private static HashMap<String, Object> getNxtUserDisplayInfo(final User user) {
        HashMap<String, Object> userInfo = new HashMap<>();
        String BASE_URL = WalletKitHelper.p.getProperty("nxt.api.url");

        String transactionsUrl = null;
        try {
            transactionsUrl = BASE_URL + "requestType=getBlockchainTransactions" + "&account="
                    + URLEncoder.encode(user.getNetworkAddress(), "UTF-8");
            LOGGER.info(transactionsUrl);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        if (transactionsUrl == null) {
            return null;
        }

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transactionsUrl);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            List<HashMap<String, Object>> result = (ArrayList<HashMap<String, Object>>) response.get("transactions");

            List<Transaction> transactions = new ArrayList<>();

            result.stream().forEach(res -> {
                if (res.get("type").toString().equals("0")) {
                    Transaction transaction = new Transaction();

                    long confirmations = Long.parseLong(res.get("confirmations").toString());
                    transaction.setConfirmations(confirmations);
                    transaction.setIndex(Integer.parseInt(res.get("transactionIndex").toString()));
                    transaction.setHash(res.get("transaction").toString());
                    Double value = Double.parseDouble(res.get("amountNQT").toString());
                    transaction.setValue(value != 0 ? value / WalletKitHelper.ONE_NXT : value);

                    if (res.get("senderRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(true);
                        transaction.setIncoming(false);
                    } else if (res.get("recipientRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(false);
                        transaction.setIncoming(true);
                    }

                    transactions.add(transaction);
                }
            });

            userInfo.put("transactions", transactions);
            getNxtBalance(userInfo, user);
            return userInfo;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    /**
     *
     * @param user
     * @param chain ARDR = 1, IGNIS = 2
     * @return
     */
    private static HashMap<String, Object> getARDRUserDisplayInfo(final User user, int chain) {
        HashMap<String, Object> userInfo = new HashMap<>();
        String BASE_URL = WalletKitHelper.p.getProperty("ardr.api.url");

        String transactionsUrl = null;
        try {
            transactionsUrl = BASE_URL + "requestType=getBlockchainTransactions&chain=" + chain + "&account="
                    + URLEncoder.encode(user.getNetworkAddress(), "UTF-8");
            LOGGER.info(transactionsUrl);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        if (transactionsUrl == null) {
            return null;
        }

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transactionsUrl);
        getRequest.setHeader("content-type", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info(s);

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            List<HashMap<String, Object>> result = (ArrayList<HashMap<String, Object>>) response.get("transactions");

            List<Transaction> transactions = new ArrayList<>();

            result.stream().forEach(res -> {
                if (res.get("type").toString().equals("0")) {
                    Transaction transaction = new Transaction();

                    long confirmations = Long.parseLong(res.get("confirmations").toString());
                    transaction.setConfirmations(confirmations);
                    transaction.setIndex(Integer.parseInt(res.get("transactionIndex").toString()));
                    if (res.containsKey("transaction")) {
                        transaction.setHash(res.get("transaction").toString());
                    } else if (res.containsKey("fxtTransaction")) {
                        transaction.setHash(res.get("fxtTransaction").toString());
                    }
                    Double value = Double.parseDouble(res.get("amountNQT").toString());
                    transaction.setValue(value != 0 ? value / WalletKitHelper.ONE_NXT : value);

                    if (res.get("senderRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(true);
                        transaction.setIncoming(false);
                    } else if (res.get("recipientRS").toString().equals(user.getNetworkAddress())) {
                        transaction.setSpent(false);
                        transaction.setIncoming(true);
                    }

                    transactions.add(transaction);
                }
            });

            userInfo.put("transactions", transactions);
            getARDRBalance(userInfo, user, chain);
            return userInfo;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    private static HashMap<String, Object> getEthereumUserDisplayInfo(final User user, User.WALLET_CODES wc) {
        HashMap<String, Object> userInfo = new HashMap<>();

        String baseUrl = "";
        String txUrl = "";
        String apiToken = WalletKitHelper.p.getProperty("etherscan.api.key");
        switch (wc) {
            case ETH:
                baseUrl = WalletKitHelper.p.getProperty("etherscan.api.url");
                txUrl = WalletKitHelper.p.getProperty("etherscan.tx.url");
                break;
            case ETHTEST:
                baseUrl = WalletKitHelper.p.getProperty("rinkeby.etherscan.api.url");
                txUrl = WalletKitHelper.p.getProperty("rinkeby.etherscan.tx.url");
                break;
            default:
                break;
        }

        String transactionsUrl = baseUrl + "?module=account&action=txlist&address=" + user.getNetworkAddress()
                + "&apikey=" + apiToken;

        LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transactionsUrl);
        getRequest.setHeader("content-type", "application/json");
        getRequest.setHeader("Accept", "application/json");

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            List<HashMap<String, Object>> result = (ArrayList<HashMap<String, Object>>) response.get("result");

            List<Transaction> transactions = new ArrayList<>();

            final String TX_URL = txUrl;
            result.stream().forEach(res -> {
                Transaction transaction = new Transaction();

                transaction.setUrl(TX_URL + res.get("hash").toString());

                long confirmations = Long.parseLong(res.get("confirmations").toString());
                transaction.setConfirmations(confirmations);
                transaction.setIndex(Integer.parseInt(res.get("transactionIndex").toString()));
                transaction.setHash(res.get("hash").toString());
                Double value = Double.parseDouble(res.get("value").toString());
                transaction.setValue(value != 0 ? value / WalletKitHelper.ONE_WEI : value);

                if (res.get("from").toString().equals(user.getNetworkAddress())
                        && res.get("txreceipt_status").toString().equals("1")) {
                    transaction.setSpent(true);
                    transaction.setIncoming(false);
                } else if (res.get("to").toString().equals(user.getNetworkAddress())
                        && res.get("txreceipt_status").toString().equals("1")) {
                    transaction.setSpent(false);
                    transaction.setIncoming(true);
                }

                transactions.add(transaction);
            });

            // https://rinkeby.etherscan.io/api?module=account&action=balance&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&tag=latest&apikey=YourApiKeyToken
            userInfo.put("transactions", transactions);
            getEthereumBalance(userInfo, wc, user);
            return userInfo;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void getEthereumBalance(HashMap<String, Object> userInfo, User.WALLET_CODES wc, User user) {
        String baseUrl = "";
        String apiToken = WalletKitHelper.p.getProperty("etherscan.api.key");
        switch (wc) {
            case ETH:
                baseUrl = WalletKitHelper.p.getProperty("etherscan.api.url");
                break;
            case ETHTEST:
                baseUrl = WalletKitHelper.p.getProperty("rinkeby.etherscan.api.url");
                break;
            default:
                break;
        }

        String transactionsUrl = baseUrl + "?module=account&action=balance&address=" + user.getNetworkAddress()
                + "&apikey=" + apiToken;

        LOGGER.log(Level.INFO, "URL: {0}", transactionsUrl);

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpGet getRequest = new HttpGet(transactionsUrl);
        getRequest.setHeader("content-type", "application/json");
        getRequest.setHeader("Accept", "application/json");

        userInfo.put("balance", 0);
        userInfo.put("received", 0);
        userInfo.put("spent", 0);
        userInfo.put("escrow", 0);

        try {
            CloseableHttpResponse httpResponse = client.execute(getRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");

            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            String result = response.get("result").toString();

            double balance = (Double.parseDouble(result) * 1.0) / WalletKitHelper.ONE_WEI;

            List<Exchange> exchanges = ExchangeDAO.getEscrowUserTrades(user);
            assert exchanges != null;
            double escrow = getEscrow(exchanges, user);

            userInfo.put("balance", balance - escrow);
            userInfo.put("escrow", escrow);
        } catch (IOException | NumberFormatException | UnsupportedOperationException e) {
            e.printStackTrace();
        }
    }

    private static double getEscrow(List<Exchange> exchanges, User user) {
        double escrow = exchanges.stream().map(x -> {
            if (!x.getStatus().equals(ExchangeDAO.STATUS.SUCCESS.toString())) {
                return (x.getAmountToSell() + x.getFees());
            } else {
                return 0;
            }
        }).collect(Collectors.summarizingDouble(Number::doubleValue)).getSum();

        exchanges = ExchangeDAO.getDebts(user);
        assert exchanges != null;
        escrow = escrow + exchanges.stream().map(x -> x.getAmountToRecieve())
                .collect(Collectors.summarizingDouble(Number::doubleValue)).getSum();

        return escrow;
    }
}
