package com.xendbit.xendserver.api;

import javax.ws.rs.Path;

@Path("admin")
public class AdminAPI {

    // protected static final Logger LOGGER = Logger.getLogger(AdminAPI.class.getName());

    // @Path("customers/activate")
    // @POST
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response activateUser(@HeaderParam("authUser") String authUser, @HeaderParam("authPassword") String authPass,
    //         User user) {
    //     LOGGER.info("Called activateUser");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("response_code", -2);
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("response_code", -1);
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         User dbUser = UserDAO.findByColumn("email", user.getEmailAddress());
    //         if (dbUser != null) {
    //             if (dbUser.isActivated()) {
    //                 res.put("response_code", -3);
    //                 res.put("response_text", "error");
    //                 res.put("result", "User is already activated");
    //             } else {
    //                 UserDAO.activateAccount(dbUser);
    //                 res.put("result", "User activated successfuly");
    //             }
    //         } else {
    //             res.put("response_code", -4);
    //             res.put("response_text", "error");
    //             res.put("result", "User with email address provided is not found");
    //         }
    //     } catch (Exception ex) {
    //         ex.printStackTrace();
    //         res.put("response_code", -5);
    //         res.put("response_text", "error");
    //         res.put("result", "Error occured activating user");
    //     }

    //     return Response.ok(res).build();
    // }

    // // deactivate customer
    // @Path("customers/deactivate")
    // @POST
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response deactivateCustomer(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, User user) {
    //     LOGGER.info("Called deactivateCustomer");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("response_code", -2);
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         } else {
    //             Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //             if (admin == null) {
    //                 res.put("response_code", -1);
    //                 res.put("result", "Authentication Failure");
    //                 res.put("response_text", "error");
    //                 return Response.ok(res).build();
    //             }

    //             User dbUser = UserDAO.findByColumn("email", user.getEmailAddress());
    //             if (dbUser != null) {
    //                 if (!dbUser.isActivated()) {
    //                     res.put("response_code", -3);
    //                     res.put("result", "User account is already deactivated");
    //                     res.put("response_text", "error");
    //                 } else {
    //                     UserDAO.deactivateAccount(dbUser);
    //                     res.put("result", "User successfully deactivated");
    //                 }
    //             } else {
    //                 res.put("response_code", -4);
    //                 res.put("result", "User with email provided does not exist");
    //                 res.put("response_text", "error");
    //             }
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", -5);
    //         res.put("response_text", "error");
    //         res.put("result", "Error occured deactivating user");
    //     }
    //     return Response.ok(res).build();
    // }

    // @Path("users")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getUsers(@HeaderParam("authUser") String authUser, @HeaderParam("authPassword") String authPass) {
    //     LOGGER.info("Called getTransactions");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         res.put("result", AdminDAO.findAllAdmins());
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting users [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // @Path("login")
    // @POST
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response login(Admin admin) {
    //     LOGGER.info("Called login");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);

    //     try {
    //         Admin dbAdmin = AdminDAO.findByUsernameAndPassword(admin.getUsername(), admin.getPassword());
    //         res.put("response_text", "success");
    //         res.put("result", dbAdmin);
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error login in [" + e.getMessage() + "]");

    //     }

    //     return Response.ok(res).build();
    // }

    // @Path("new")
    // @POST
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response create(Admin admin) {
    //     LOGGER.info("Called Crate New Admin");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         AdminDAO.createAdmin(admin);
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error creating admin [" + e.getMessage() + "]");

    //     }
    //     return Response.ok(res).build();
    // }

    // /**
    //  * Customers API Start
    //  */
    // @Path("customers/{page}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomersByPage(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("page") String page) {
    //     LOGGER.info("Called getCustomers");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         List<User> users = UserDAO.findByPage(page);
    //         res.put("page", page);
    //         res.put("result", users);
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }

    //     return Response.ok(res).build();
    // }

    // @Path("customers/total")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getTotalCustomers(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass) {
    //     LOGGER.info("Called getTotalCustomers");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         res.put("result", UserDAO.findAll().size());

    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // @Path("customers")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomers(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass) {
    //     LOGGER.info("Called getCustomers");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         List<User> users = UserDAO.findAll();
    //         res.put("result", users);
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();

    // }

    // // get a single customer by email address
    // @Path("customer/email/{emailAddress}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomer(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPassword, @PathParam("emailAddress") String emailAddress) {
    //     LOGGER.info("Called getCustomer");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPassword == null) {
    //             res.put("response_code", -1);
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPassword);
    //         LOGGER.info(authUser + ' ' + authPassword);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         String columnName = "EMAIL";
    //         User user = UserDAO.findByColumn(columnName, emailAddress);
    //         if (user != null) {
    //             res.put("result", user);
    //             res.put("response_code", 0);
    //             res.put("response_text", "sucess");
    //         } else {
    //             res.put("response_code", -5);
    //             res.put("response_text", "error");
    //             res.put("result", "Customer does not exist");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get a single customer by ID
    // @Path("customer/id/{id}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomerById(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPassword, @PathParam("id") String id) {
    //     LOGGER.info("Called getCustomer");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPassword == null) {
    //             res.put("response_code", -1);
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPassword);
    //         LOGGER.info(authUser + ' ' + authPassword);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         User user = UserDAO.findById(Integer.parseInt(id));
    //         if (user != null) {
    //             res.put("result", user);
    //             res.put("response_code", 0);
    //             res.put("response_text", "sucess");
    //         } else {
    //             res.put("response_code", -5);
    //             res.put("response_text", "error");
    //             res.put("result", "Customer does not exist");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get customer by date limit by page
    // @Path("customers/date/{startDate}/{endDate}/page/{pageNumber}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomersByDateByPage(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("startDate") long startDate,
    //         @PathParam("endDate") long endDate, @PathParam("pageNumber") String pageNumber) {
    //     LOGGER.info("Called get Customers By Date and by Page");
    //     Map<String, Object> res = new HashMap<>();

    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         List<User> customers = UserDAO.findByDateAndPage(startDate, endDate, pageNumber);
    //         if (customers != null) {
    //             res.put("result", customers);
    //         } else {
    //             res.put("result", "There are no customers registered within the date range spacified");
    //         }

    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get customer KYC Information
    // @Path("customer/kyc/{emailAddress}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomerKYC(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("emailAddress") String email) {
    //     Map<String, Object> res = new HashMap<>();

    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {

    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);

    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         String columnName = "EMAIL";
    //         User user = UserDAO.findByColumn(columnName, email);
    //         if (user != null) {
    //             Map<String, Object> accountKYC = BlockchainHelper.getAccountKYC(user);
    //             res.put("result", accountKYC);
    //         } else {
    //             res.put("result", "Account KYC does not exist for the Customer");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();

    // }

    // /**
    //  * Customers API End
    //  */

    // /**
    //  * Date must be in long format Date.getTime()
    //  */

    // /**
    //  * New Transactions API
    //  */

    // // get all transactions by page
    // @Path("exchange/transactions/{page}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getTransactionsBypage(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPassword, @PathParam("page") String page) {
    //     LOGGER.info("getTransactionsByPage called");

    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPassword == null) {
    //             res.put("response_code", -1);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Header parameters do not exist");
    //             return Response.ok(res).build();
    //         }

    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPassword);
    //         if (admin == null) {
    //             res.put("response_code", -2);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Invalid params");
    //             return Response.ok(res).build();
    //         }

    //         List<Exchange> transactions = ExchangeDAO.findTransactionsByPage(page);
    //         if (transactions != null) {
    //             res.put("result", transactions);
    //         } else {
    //             res.put("response_code", -3);
    //             res.put("error_text", "success");
    //             res.put("result", "There are no transactions currently existing");

    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting transactions [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get all transactions by date limit by page
    // @Path("exchange/transactions/date/{startDate}/{endDate}/page/{pageNumber}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getTransactionsByDateLimitByPage(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("startDate") long startDate,
    //         @PathParam("endDate") long endDate, @PathParam("pageNumber") String page) {
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("response_code", -1);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Header parameters do not exist");
    //             return Response.ok(res).build();
    //         }

    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("response_code", -2);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Invalid params");
    //             return Response.ok(res).build();
    //         }
    //         List<Exchange> transactions = ExchangeDAO.findTransactionsByDateAndPage(page, startDate, endDate);
    //         if (transactions != null) {
    //             res.put("result", transactions);
    //         } else {
    //             res.put("response_code", -3);
    //             res.put("error_text", "success");
    //             res.put("result", "There are no transactions currently existing");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting transactions [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();

    // }

    // // get all sell transactions of a customer by page
    // @Path("exchange/sell/customer/{emailAddress}/transactions/{page}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getSellTransactionsByCustomer(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPassword, @PathParam("emailAddress") String email,
    //         @PathParam("page") String page) {
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPassword == null) {
    //             res.put("response_code", -1);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Header parameters do not exist");
    //             return Response.ok(res).build();
    //         }

    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPassword);
    //         if (admin == null) {
    //             res.put("response_code", -2);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Invalid params");
    //             return Response.ok(res).build();
    //         }
    //         String userColumnName = "EMAIL";
    //         User user = UserDAO.findByColumn(userColumnName, email);
    //         if (user != null) {
    //             String columnName = "SELLER_ID";
    //             List<Exchange> transactions = ExchangeDAO.findByColumnNameAndPage(columnName, user.getId(), page);
    //             if (transactions != null) {
    //                 res.put("result", transactions);
    //             } else {
    //                 res.put("response_code", -3);
    //                 res.put("error_text", "success");
    //                 res.put("result", "There are no transactions currently existing");
    //             }
    //         } else {
    //             res.put("response_code", -4);
    //             res.put("result", "Customer account does not exist");
    //             res.put("response_text", "error");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting transactions [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get all buy transactions of a customer by page
    // @Path("exchange/buy/customer/{emailAddress}/transactions/{page}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getBuyTransactionsByCustomer(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPassword, @PathParam("emailAddress") String email,
    //         @PathParam("page") String page) {
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPassword == null) {
    //             res.put("response_code", -1);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Header parameters do not exist");
    //             return Response.ok(res).build();
    //         }

    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPassword);
    //         if (admin == null) {
    //             res.put("response_code", -2);
    //             res.put("error_text", "error");
    //             res.put("result", "Authentication Failure. Invalid params");
    //             return Response.ok(res).build();
    //         }

    //         String userColumnName = "EMAIL";
    //         User user = UserDAO.findByColumn(userColumnName, email);
    //         if (user != null) {
    //             String columnName = "BUYER_ID";
    //             List<Exchange> transactions = ExchangeDAO.findByColumnNameAndPage(columnName, user.getId(), page);
    //             if (transactions != null) {
    //                 res.put("result", transactions);
    //             } else {
    //                 res.put("response_code", -3);
    //                 res.put("error_text", "success");
    //                 res.put("result", "There are no transactions currently existing");
    //             }
    //         } else {
    //             res.put("response_code", -4);
    //             res.put("result", "Customer account does not exist");
    //             res.put("response_text", "error");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting transactions [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get customer sell transaction by date by page
    // @Path("exchange/transactions/customer/sell/{emailAddress}/{startDate}/{endDate}/page/{pageNumber}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomerSellTransactionsByDateAndPage(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("emailAddress") String emailAddress,
    //         @PathParam("startDate") long startDate, @PathParam("endDate") long endDate,
    //         @PathParam("pageNumber") String pageNumber) {
    //     LOGGER.info("get Customer Transactions by Date and Page called");
    //     Map<String, Object> res = new HashMap<>();

    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         String userColumnName = "EMAIL";
    //         User user = UserDAO.findByColumn(userColumnName, emailAddress);
    //         if (user != null) {
    //             String columnName = "SELLER_ID";
    //             List<Exchange> transactions = ExchangeDAO.findByColumnNameByDateByPage(columnName, user.getId(),
    //                     startDate, endDate, pageNumber);
    //             if (transactions != null) {
    //                 res.put("result", transactions);
    //             } else {
    //                 res.put("response_code", -3);
    //                 res.put("error_text", "success");
    //                 res.put("result", "There are no transactions currently existing");
    //             }
    //         } else {
    //             res.put("response_code", -4);
    //             res.put("result", "Customer account does not exist");
    //             res.put("response_text", "error");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting transactions [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get customer sell transaction by date by page
    // @Path("exchange/transactions/customer/buy/{emailAddress}/{startDate}/{endDate}/page/{pageNumber}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getCustomerBuyTransactionsByDateAndPage(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("emailAddress") String emailAddress,
    //         @PathParam("startDate") long startDate, @PathParam("endDate") long endDate,
    //         @PathParam("pageNumber") String pageNumber) {
    //     LOGGER.info("get Customer Transactions by Date and Page called");
    //     Map<String, Object> res = new HashMap<>();

    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         String userColumnName = "EMAIL";
    //         User user = UserDAO.findByColumn(userColumnName, emailAddress);
    //         if (user != null) {
    //             String columnName = "BUYER_ID";
    //             List<Exchange> transactions = ExchangeDAO.findByColumnNameByDateByPage(columnName, user.getId(),
    //                     startDate, endDate, pageNumber);
    //             if (transactions != null) {
    //                 res.put("result", transactions);
    //             } else {
    //                 res.put("response_code", -3);
    //                 res.put("error_text", "success");
    //                 res.put("result", "There are no transactions currently existing");
    //             }
    //         } else {
    //             res.put("response_code", -4);
    //             res.put("result", "Customer account does not exist");
    //             res.put("response_text", "error");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting transactions [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // get all transactions by date by page

    // /**
    //  * Date must be in long format Date.getTime()
    //  */
    // @Path("exchange/transactions/all/{startDate}/{endDate}/page/{page}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getAllTransactions(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("startDate") long startDate,
    //         @PathParam("endDate") long endDate, @PathParam("page") String page) {
    //     LOGGER.info("Called getTransactions");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         List<Exchange> transactions = ExchangeDAO.findTransactionsByDateAndPage(page, startDate, endDate);
    //         if (transactions != null) {
    //             res.put("result", transactions);
    //         } else {
    //             res.put("response_code", -3);
    //             res.put("error_text", "success");
    //             res.put("result", "There are no transactions currently existing");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // /**
    //  * Transactions Status Types API
    //  */

    // @Path("exchange/transactions/statustype")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getAllTransactionStatusTypes(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass) {
    //     LOGGER.info("Called getTransactionStatusTypes");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         List<TransactionStatus> statusTypes = ExchangeDAO.getTransactionStatusTypes();
    //         if (statusTypes != null) {
    //             res.put("result", statusTypes);
    //         } else {
    //             res.put("response_code", -3);
    //             res.put("error_text", "success");
    //             res.put("result", "There are no transactions status types currently existing");
    //         }
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // add new transaction status
    // @Path("exchange/transactions/new/statustype")
    // @POST
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response addTransactionStatusType(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, TransactionStatus request) {
    //     LOGGER.info("Called getTransactionStatusTypes");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         TransactionStatus statusSearch = ExchangeDAO.getTransactionStatusByType(request.getStatusType());

    //         if (statusSearch != null) {
    //             res.put("result", "This status type already exist in the database");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         String sqlResponse = ExchangeDAO.addTransactionStatusType(request);

    //         res.put("result", sqlResponse);
    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // // delete a transaction status
    // @Path("exchange/transactions/delete/statustype/{status}")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response deleteTransactionStatusType(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPass, @PathParam("status") String status) {
    //     LOGGER.info("Called getTransactionStatusTypes");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {
    //         if (authUser == null || authPass == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }
    //         Admin admin = AdminDAO.findByUsernameAndPassword(authUser, authPass);
    //         if (admin == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         boolean sqlResponse = ExchangeDAO.deleteTransactionStatusType(status);

    //         if (!sqlResponse) {
    //             res.put("result", "SQL Error: Incomplete SQL Operation");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", "Server Error gettting customer [" + e.getMessage() + "]");
    //         e.printStackTrace();
    //     }
    //     return Response.ok(res).build();
    // }

    // @Path("group/add")
    // @POST
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response addNewGroup(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPassword, Group request) {
    //     LOGGER.info("Add new group called");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {

    //         if (authUser == null || authPassword == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         AdminDAO.createGroup(request);
    //         res.put("response_code", 1);
    //         res.put("result", "New group created");

    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", e.getMessage());
    //     }
    //     return Response.ok(res).build();
    // }

    // @Path("groups")
    // @GET
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response getGroups(@HeaderParam("authUser") String authUser,
    //         @HeaderParam("authPassword") String authPassword) {
    //     LOGGER.info("Get all groups called");
    //     Map<String, Object> res = new HashMap<>();
    //     res.put("response_code", 0);
    //     res.put("response_text", "success");

    //     try {

    //         if (authUser == null || authPassword == null) {
    //             res.put("result", "Authentication Failure");
    //             res.put("response_text", "error");
    //             return Response.ok(res).build();
    //         }

    //         List<Group> groups = AdminDAO.findAllGroups();

    //         res.put("result", groups);

    //     } catch (Exception e) {
    //         res.put("response_code", 0);
    //         res.put("response_text", "error");
    //         res.put("result", e.getMessage());
    //     }
    //     return Response.ok(res).build();
    // }
}
