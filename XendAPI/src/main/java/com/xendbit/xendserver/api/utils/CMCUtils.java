/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.api.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;

import com.xendbit.xendserver.models.utils.DAOUtils;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author aardvocate
 */
public class CMCUtils {
  static final Logger LOG = Logger.getLogger(CMCUtils.class.getName());
  static final ObjectMapper OM = new ObjectMapper();

  public static CloseableHttpClient registerHttps() {
    int timeout = 60000;
    RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000)
        .setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();
    CloseableHttpClient client = null;
    try {
      SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true).build();

      client = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(new NoopHostnameVerifier())
          .setDefaultRequestConfig(config).build();
    } catch (Exception e) {
      LOG.log(Level.SEVERE, e.getMessage(), e);
      e.printStackTrace();
    }
    return client;
  }

  @SuppressWarnings("unchecked")
  public static HashMap<String, Object> getPrice(String token, String currency, String amount) throws Exception {
    String apiKey = DAOUtils.p.getProperty("cmc.api.key", "94e6a38c-2412-470b-8704-00ad9fa9351b");
    String url = DAOUtils.p.getProperty("cmc.url", "https://pro-api.coinmarketcap.com/v1/tools/price-conversion");
    LOG.info(url);
    List<NameValuePair> parameters = new ArrayList<>();
    parameters.add(new BasicNameValuePair("symbol", token));
    parameters.add(new BasicNameValuePair("amount", amount));
    parameters.add(new BasicNameValuePair("convert", currency));

    URIBuilder query = new URIBuilder(url);
    query.addParameters(parameters);

    CloseableHttpClient client = registerHttps();
    HttpGet getRequest = new HttpGet(query.build());
    getRequest.setHeader("content-type", "application/json");
    getRequest.setHeader("Accept", "application/json");
    getRequest.setHeader("X-CMC_PRO_API_KEY", apiKey);

    CloseableHttpResponse httpResponse = client.execute(getRequest);
    InputStream is = httpResponse.getEntity().getContent();
    String s = IOUtils.toString(is, "UTF-8");
    LOG.info("CMC Response String = " + s);
    HashMap<String, Object> response = OM.readValue(s, HashMap.class);
    HashMap<String, Object> data = (HashMap<String, Object>) response.get("data");
    HashMap<String, Object> quote = (HashMap<String, Object>) data.get("quote");
    HashMap<String, Object> cur = (HashMap<String, Object>) quote.get(currency);
    LOG.info(cur + "");
    return cur;
  }

  public static void main(String args[]) throws Exception {
    HashMap<String, Object> cmcData = CMCUtils.getPrice("ARDR", "USD", "1");
    System.out.println(cmcData);
    double price = Double.parseDouble(cmcData.get("price").toString());
    double markupRate = Double.parseDouble(WalletKitHelper.p.getProperty("xend.markup")) / 100.0;
    double highPlus3Percent = price + (markupRate * price);

    System.out.println(highPlus3Percent);
  }
}
