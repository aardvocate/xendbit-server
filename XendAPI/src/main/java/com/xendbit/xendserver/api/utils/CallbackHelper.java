package com.xendbit.xendserver.api.utils;

public interface CallbackHelper {
    void success(AppMessenger appMessenger);
    void error(AppMessenger appMessenger);
}
