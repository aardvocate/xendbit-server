package com.xendbit.xendserver.api.utils;

import com.xendbit.xendserver.models.SendObject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

@SuppressWarnings("unchecked")
public class MoneyWaveAPI {

    private static final Logger LOGGER = Logger.getLogger(MoneyWaveAPI.class.getName());

    public static String authToken = null;
    public static Date tokenTime = null;

    public static String authenticate() throws XendException {
        if (authToken == null) {
            String url = WalletKitHelper.p.getProperty("flutterwave.base.url") + "/merchant/verify";
            LOGGER.info("URL: " + url);

            HashMap<String, Object> fields = new HashMap<>();
            fields.put("apiKey", WalletKitHelper.FLUTTERWAVE_API_KEY);
            fields.put("secret", WalletKitHelper.FLUTTERWAVE_API_SECRET);

            CloseableHttpClient client = WalletKitHelper.registerHttps();
            HttpPost postRequest = new HttpPost(url);
            postRequest.setHeader("content-type", "application/json");
            postRequest.setHeader("Accept", "application/json");

            try {
                String jsonString = WalletKitHelper.OM.writeValueAsString(fields);
                StringEntity entity = new StringEntity(jsonString);
                entity.setContentType("application/json");

                postRequest.setEntity(entity);
                CloseableHttpResponse httpResponse = client.execute(postRequest);
                InputStream is = httpResponse.getEntity().getContent();
                String s = IOUtils.toString(is, "UTF-8");
                LOGGER.info("Response String = " + s);
                HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
                LOGGER.info(response.toString());

                if (httpResponse.getStatusLine().getStatusCode() != 200) {
                    throw new XendException(response.get("status") + " response recieved from FW");
                }

                if (response.get("status").equals("success")) {
                    authToken = response.get("token").toString();
                    tokenTime = new Date();
                    return authToken;
                } else {
                    throw new XendException(response.get("status") + " response recieved from FW");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            long thirtyMinutes = 30 * 60 * 1000;
            Date tokenTimePlusThirtyMinutes = new Date(tokenTime.getTime() + thirtyMinutes);
            Date now = new Date();

            if (now.after(tokenTimePlusThirtyMinutes)) {
                authToken = null;
                tokenTime = null;
                return authenticate();
            } else {
                return authToken;
            }
        }
    }

    public static String verifyBankAccount(SendObject sendObject) throws XendException {
        if (sendObject.getBankCode().equals("000")) {
            return "Evaluation";
        }
        String url = WalletKitHelper.p.getProperty("flutterwave.base.url") + "/resolve/account";
        LOGGER.info("URL: " + url);

        HashMap<String, Object> fields = new HashMap<>();

        fields.put("account_number", sendObject.getAccountNumber());
        fields.put("bank_code", sendObject.getBankCode());

        CloseableHttpClient client = WalletKitHelper.registerHttps();
        HttpPost postRequest = new HttpPost(url);
        postRequest.setHeader("content-type", "application/json");
        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Authorization", authenticate());

        String jsonString = null;
        try {
            jsonString = WalletKitHelper.OM.writeValueAsString(fields);

            StringEntity entity = new StringEntity(jsonString);
            entity.setContentType("application/json");

            postRequest.setEntity(entity);
            CloseableHttpResponse httpResponse = client.execute(postRequest);
            InputStream is = httpResponse.getEntity().getContent();
            String s = IOUtils.toString(is, "UTF-8");
            LOGGER.info("Response String = " + s);
            HashMap<String, Object> response = WalletKitHelper.OM.readValue(s, HashMap.class);
            LOGGER.info(response.toString());

            if (response.get("status").toString().equals("error")) {
                return "Evaluation Account";
            }

            return ((HashMap<String, Object>) response.get("data")).get("account_name").toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
