/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xendbit.xendserver.listener;



import com.xendbit.xendserver.api.utils.WalletKitHelper;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Web application lifecycle listener.
 *
 * @author aardvocate
 */

public class MyListener implements ServletContextListener {

    private final Logger LOGGER = Logger.getLogger(MyListener.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.info("Starting Listener. ");

        boolean alreadyStarted = false;
        try (Socket client = new Socket("127.0.0.1", 18081)) {
            alreadyStarted = true;
            client.getOutputStream().write("echo".getBytes());
            client.close();
        } catch (IOException e) {
            alreadyStarted = false;
        }

        if (!alreadyStarted) {
            new Thread() {
                @Override
                public void run() {
                    ServerSocket socket = null;
                    try {
                        socket = new ServerSocket(18081);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    while (true) {
                        if (socket.isClosed()) {
                            break;
                        }
                        Socket s = null;
                        try {
                            s = socket.accept();
                            InputStream is = s.getInputStream();
                            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                            String line = reader.readLine();

                            if (line.equals("reload_properties")) {
                                try {
                                    WalletKitHelper.p = new Properties();
                                    WalletKitHelper.p.load(new FileInputStream(new File("/etc/xendbit/xendbit.properties")));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                LOGGER.info(line);
                                socket.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.info("Deregistering SQL-Drivers ...");
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                LOGGER.info("Error deregistering driver " + driver.getClass().getName());
            }
        }
    }

}
