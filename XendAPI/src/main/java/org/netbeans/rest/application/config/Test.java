/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;


import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 *
 * @author aardvocate
 */
public class Test {
    public static void main(String[] args) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest("hazard peace revamp ruffled pact rhythm hatred hill myth bee rub suede suede".getBytes(StandardCharsets.UTF_8));
            String hash = Base64.getEncoder().encodeToString(encodedhash);
            System.out.println(hash);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }        
    }
}
