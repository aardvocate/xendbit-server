#!/bin/bash

mvn clean install
if [[ "$?" -ne 0 ]] ; then
  echo 'Maven Command Failed. Will not deploy'; exit 666
fi

docker cp XendAPI/target/XendAPI-1.0.war prow:/usr/local/tomcat/webapps/api.war
docker cp BitcoinAPI/target/BitcoinAPI-1.0.war prow:/usr/local/tomcat/webapps/chain.war
docker cp XendMailer/target/XendMailer-1.0.war prow:/usr/local/tomcat/webapps/emailer.war
docker cp XendImager/target/XendImager-1.0.war prow:/usr/local/tomcat/webapps/imager.war
docker cp XendNotificationService/target/XendNotificationService-1.0.war prow:/usr/local/tomcat/webapps/notify.war
